--Name:     MPU9250
--Author:   Radoslaw Kalinka
--Date:     2017.06.04
--Desc:     MPU9250 Driver Module 
--          Module works with 50MHz input clock
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_misc.all;
use ieee.numeric_std.ALL;
library work;
use work.regs.all;

entity MPU9250 is
   PORT(
         i_clk       : IN  STD_LOGIC;
         i_clk5M     : IN  STD_LOGIC;
         i_rst       : IN  STD_LOGIC;
         iv_DataIn   : IN  std_logic_vector(111 downto 0);
         i_SPIBsy    : IN  std_logic;
         i_SPIDrdy   : IN  std_logic;
         ov_DataOut  : OUT std_logic_vector(15 downto 0);
         ov_BitLen   : OUT std_logic_vector(7 downto 0);
         ov_DataLen  : OUT std_logic_vector(7 downto 0);
         o_SPIStart  : OUT std_logic;
         ov_angx     : OUT s2q16;
         ov_angy     : OUT s2q16;
         ov_angz     : OUT s2q16;
         o_DRDY      : OUT std_logic                               --Goes low at new valid data recognition
      );
end;

architecture bhv of MPU9250 is
   attribute keep: boolean;
   type state_type is (who_am_I_ask, mpu9250_retry,  disI2C, enable_sensors, wait_for_wakeup, clock_select, wait_for_clock, config, gyro_config, acc1_config, acc2_config, MPU_read, GyroAngLimit, GyroXScale, GyroYScale, GyroZScale, AccXSqare, AccYSqare, AccZSqare, AccSquareSum, AccYAngCalc, AccXAngCalc, AccYRadToDeg, AccXRadToDeg, SensorFusion, SensorXOut, SensorYOut, SensorZOut, wait_for_new_data);
   signal state                     : state_type := who_am_I_ask;
   signal cnt                       : integer range 0 to 10000000;
   signal cntoff                    : integer range 0 to 1024;
   signal DataOut                   : std_logic_vector(15 downto 0);
   signal BitLen                    : std_logic_vector(7 downto 0);
   signal DataLen                   : std_logic_vector(7 downto 0);
   signal SPIStart                  : std_logic;
   signal accx, accy, accz          : s0q16;
   signal accx2, accy2, accz2       : s16q16;
   signal accxang, accyang          : s16q16;
   signal accxang0, accyang0          : s16q16;
   signal accxang1, accyang1          : s16q16;
   signal accxang2, accyang2          : s16q16;
   signal accxang3, accyang3          : s16q16;
   signal accxang4, accyang4          : s16q16;
   signal accxang5, accyang5          : s16q16;
   signal accxang6, accyang6          : s16q16;
   signal accxang7, accyang7          : s16q16;
   signal accxangsum, accyangsum          : s16q16;
   signal angx, angy, angz          : s16q16;
   signal angx_o, angy_o, angz_o    : s2q16;
   signal gyrxoff, gyryoff, gyrzoff : s0q16;
   signal sqrt_radical              : std_logic_vector(31 downto 0);
   signal sqrt_q                    : std_logic_vector(15 downto 0);
   signal atan2_q                   : std_logic_vector(12 downto 0);
   signal atan2_x                   : std_logic_vector(15 downto 0);
   signal atan2_y                   : std_logic_vector(15 downto 0);
   signal DRDY                      : std_logic;
   
   attribute keep of angx: signal is true;
   attribute keep of angy: signal is true;
   attribute keep of angz: signal is true;
   
begin

--   sqrt_inst: ENTITY work.sqrt(SYN)
--   PORT MAP
--   (
--      aclr        => i_rst,
--      clk         => i_clk5M,
--      radical     => sqrt_radical,
--      q           => sqrt_q,
--      remainder   => open
--   );
   
--   cordic_atan2_inst: entity work.cordic_atan2(rtl)
--   port map(
--      areset      => i_rst,   -- areset.reset
--      clk         => i_clk5M, -- clk.clk
--      q           => atan2_q, -- q.q
--      x           => atan2_x, -- x.x
--      y           => atan2_y  -- y.y
--   );

   ov_DataOut  <= DataOut;
   ov_BitLen   <= BitLen;
   ov_DataLen  <= DataLen;
   o_SPIStart  <= SPIStart;
   ov_angx     <= angx_o;
   ov_angy     <= angy_o;
   ov_angz     <= angz_o;
   o_DRDY      <= DRDY;
   
   process(i_clk, i_rst)
   begin
      if(i_rst = '1') then
         state          <= who_am_I_ask;
         cnt            <= 0;
         cntoff         <= 0;
         DataOut        <= (others => '0');
         BitLen         <= (others => '0');
         DataLen        <= (others => '0');
         SPIStart       <= '0';
         accx           <= (others => '0');
         accy           <= (others => '0');
         accz           <= (others => '0');
         accx2          <= (others => '0');
         accy2          <= (others => '0');
         accz2          <= (others => '0');
         accxang        <= (others => '0');
         accyang        <= (others => '0');
         accxang0       <= (others => '0');
         accyang0       <= (others => '0');
         accxang1       <= (others => '0');
         accyang1       <= (others => '0');
         accxang2       <= (others => '0');
         accyang2       <= (others => '0');
         accxang3       <= (others => '0');
         accyang3       <= (others => '0');
         accxang4       <= (others => '0');
         accyang4       <= (others => '0');
         accxang5       <= (others => '0');
         accyang5       <= (others => '0');
         accxang6       <= (others => '0');
         accyang6       <= (others => '0');
         accxang7       <= (others => '0');
         accyang7       <= (others => '0');
         accxangsum     <= (others => '0');
         accyangsum     <= (others => '0');
         angx           <= (others => '0');
         angy           <= (others => '0');
         angz           <= (others => '0');
         angx_o         <= (others => '0');
         angy_o         <= (others => '0');
         angz_o         <= (others => '0');
         gyrxoff        <= (others => '0');
         gyryoff        <= (others => '0');
         gyrzoff        <= (others => '0');
         sqrt_radical   <= (others => '0');
         atan2_x        <= (others => '0');
         atan2_y        <= (others => '0');
         DRDY           <= '0';
      elsif(rising_edge(i_clk)) then
         case state is
            when who_am_I_ask =>
               DataOut  <= "1111010100000000";  --who_am_I(117) read
               DataLen  <= "00010000";          --16bit transaction
               BitLen   <= "00110010";          --1MHz (1/50)
               DRDY     <= '0';
               
               if(i_SPIDrdy = '0') then
                  if(i_SPIBsy = '0') then
                     SPIStart <= '1';
                  else
                     SPIStart <= '0';
                  end if;
               else
                  if(iv_DataIn(7 downto 0) = "01110001") then
                     state <= disI2C;
                  else
                     state <= mpu9250_retry;
                  end if;
                  cnt      <= 0;
               end if;
               
            when mpu9250_retry =>
               if(cnt < 50000) then
                  cnt <= cnt + 1;
               else
                  state <= who_am_I_ask;
               end if;
               
            when disI2C =>
               DataOut  <= "0110101000010000";  --User Control 106 I2C Disable bit[4]
               DataLen  <= "00010000";          --16bit transaction
               BitLen   <= "00110010";          --1MHz (1/50)
               
               if(i_SPIDrdy = '0') then
                  if(i_SPIBsy = '0') then
                     SPIStart <= '1';
                  else
                     SPIStart <= '0';
                  end if;
               else
                  --DataIn   <= iv_DataIn;
                  state    <= enable_sensors;
                  cnt      <= 0;
               end if;
               
            when enable_sensors =>
               DataOut  <= "0110101100000000";  --Power Mgmt 107 Clear sleep mode bit[6] enable all sensors
               DataLen  <= "00010000";          --16bit transaction
               BitLen   <= "00110010";          --1MHz (1/50)
               
               if(i_SPIDrdy = '0') then
                  if(i_SPIBsy = '0') then
                     SPIStart <= '1';
                  else
                     SPIStart <= '0';
                  end if;
               else
                  --DataIn   <= iv_DataIn;
                  state    <= wait_for_wakeup;
                  cnt      <= 0;
               end if;
               
            when wait_for_wakeup =>
               if(cnt < 5000000) then
                  cnt <= cnt + 1;
               else
                  state <= clock_select;
               end if;
               
            when clock_select =>
               DataOut  <= "0110101100000001";  --Power Mgmt 107 Auto selects the best available clock source â€“ PLL if ready, else use the Internal oscillator
               DataLen  <= "00010000";          --16bit transaction
               BitLen   <= "00110010";          --1MHz (1/50)
               
               if(i_SPIDrdy = '0') then
                  if(i_SPIBsy = '0') then
                     SPIStart <= '1';
                  else
                     SPIStart <= '0';
                  end if;
               else
                  --DataIn   <= iv_DataIn;
                  state    <= wait_for_clock;
                  cnt      <= 0;
               end if;
            
            when wait_for_clock =>
               if(cnt < 10000000) then
                  cnt <= cnt + 1;
               else
                  state <= config;
               end if;
            
            when config =>
               DataOut  <= "0001101000000000";  --Config 26 FIFO off [0], DLPF 0 [2-0]
               DataLen  <= "00010000";          --16bit transaction
               BitLen   <= "00110010";          --1MHz (1/50)
               
               if(i_SPIDrdy = '0') then
                  if(i_SPIBsy = '0') then
                     SPIStart <= '1';
                  else
                     SPIStart <= '0';
                  end if;
               else
                  --DataIn   <= iv_DataIn;
                  state    <= gyro_config;
                  cnt      <= 0;
               end if;
               
            when gyro_config =>
               DataOut  <= "0001101100011010";  --Gyro_Config 27 Fs=32kHz, Bandwidth=3600Hz, 
               DataLen  <= "00010000";          --16bit transaction
               BitLen   <= "00110010";          --1MHz (1/50)
               
               if(i_SPIDrdy = '0') then
                  if(i_SPIBsy = '0') then
                     SPIStart <= '1';
                  else
                     SPIStart <= '0';
                  end if;
               else
                  --DataIn   <= iv_DataIn;
                  state    <= acc1_config;
                  cnt      <= 0;
               end if;
               
            when acc1_config =>
               DataOut  <= "0001110000011000";  --Acc_Config1 28 FullScale 16g [3,4] 
               DataLen  <= "00010000";          --16bit transaction
               BitLen   <= "00110010";          --1MHz (1/50)
               
               if(i_SPIDrdy = '0') then
                  if(i_SPIBsy = '0') then
                     SPIStart <= '1';
                  else
                     SPIStart <= '0';
                  end if;
               else
                  --DataIn   <= iv_DataIn;
                  state    <= acc2_config;
                  cnt      <= 0;
               end if;
               
            when acc2_config =>
               DataOut  <= "0001110100000010";  --Acc_Config2 29 FullScale ODR=1kHz, DLPF=10.2Hz
               DataLen  <= "00010000";          --16bit transaction
               BitLen   <= "00110010";          --1MHz (1/50)
               
               if(i_SPIDrdy = '0') then
                  if(i_SPIBsy = '0') then
                     SPIStart <= '1';
                  else
                     SPIStart <= '0';
                  end if;
               else
                  --DataIn   <= iv_DataIn;
                  state    <= MPU_read;
                  cnt      <= 0;
               end if;
               
            when MPU_read =>
               DataOut  <= "1011101100000000";  --Acc Data + read(0x80)
               DataLen  <= "01111000";          --56 (3x16 + 1x16 + 3x16 + 8)bit transaction
               BitLen   <= "00000100";          --12.5MHz (1/4)
               DRDY     <= '0';
               
               if(i_SPIDrdy = '0') then
                  if(i_SPIBsy = '0') then
                     SPIStart <= '1';
                  else
                     SPIStart <= '0';
                  end if;
               else
                  accx  <= signed(iv_DataIn(111 downto 96));
                  accy  <= signed(iv_DataIn(95  downto 80));
                  accz  <= signed(iv_DataIn(79  downto 64));
                  --temp <= signed(iv_DataIn(63 downto 48));
                  
                  if(cntoff < 1024) then
                     gyrxoff <= gyrxoff + resize(signed(iv_DataIn(47 downto 32)), gyrxoff'length);
                     gyryoff <= gyryoff + resize(signed(iv_DataIn(31 downto 16)), gyryoff'length);
                     gyrzoff <= gyrzoff + resize(signed(iv_DataIn(15 downto  0)), gyrzoff'length);
                     cntoff  <= cntoff + 1;
                     state   <= wait_for_new_data;
                  else
                     angx  <= angx + resize(signed(iv_DataIn(47 downto 32)), angx'length) - shift_right(gyrxoff, 10);
                     angy  <= angy + resize(signed(iv_DataIn(31 downto 16)), angy'length) - shift_right(gyryoff, 10);
                     angz  <= angz + resize(signed(iv_DataIn(15 downto  0)), angz'length) - shift_right(gyrzoff, 10);
                     state <= GyroAngLimit;
                  end if;
                  
                  cnt <= 0;
               end if;
               
            when GyroAngLimit =>                      --23592960 is 180 deg
               if(angx > 23592960) then
                  angx <= angx - to_signed(47185920, angx'length);
               elsif(angx < -23592960) then
                  angx <= angx + to_signed(47185920, angx'length);
               end if;
               if(angy > 23592960) then
                  angy <= angy - to_signed(47185920, angy'length);
               elsif(angy < -23592960) then
                  angy <= angy + to_signed(47185920, angy'length);
               end if;
               if(angz > 23592960) then
                  angz <= angz - to_signed(47185920, angz'length);
               elsif(angz < -23592960) then
                  angz <= angz + to_signed(47185920, angz'length);
               end if;
               cnt   <= cnt + 1;
               state <= GyroXScale;
               
            when GyroXScale =>
               angx  <= resize(angx*63, angx'length);
               cnt   <= cnt + 1;
               state <= GyroYScale;
            
            when GyroYScale =>
               angy  <= resize(angy*63, angy'length);
               cnt   <= cnt + 1;
               state <= AccXSqare;
               
            when AccXSqare =>
               accx2 <= accx * accx;
               cnt   <= cnt + 1;
               state <= AccYSqare;
            
            when AccYSqare =>
               accy2 <= accy * accy;
               cnt   <= cnt + 1;
               state <= AccZSqare;
            
            when AccZSqare =>
               accz2 <= accz * accz;
               cnt   <= cnt + 1;
               state <= AccSquareSum;
			   
			   when AccSquareSum =>
			      sqrt_radical <= std_logic_vector(accx2 + accy2 + accz2);
               
            when AccYAngCalc =>
               sqrt_radical <= std_logic_vector(accy2 + accz2);
--               if(accz >= 0) then
--                  atan2_x   <= std_logic_vector(signed(sqrt_q));
--               else
--                  atan2_x   <= std_logic_vector(0 - signed(sqrt_q));
--               end if;
               atan2_x  <= std_logic_vector(accz);
               atan2_y  <= std_logic_vector(0 - accx);
--               accyang  <= resize(signed(atan2_q&"000000"), accyang'length);
               --accyang <= resize(shift_right(resize(signed(atan2_q&"000000"), 32)*7509888, 16), accyang'length);
               cnt      <= cnt + 1;
               if(cnt > 80) then
                  state <= AccXAngCalc;
               end if;
               
            when AccXAngCalc =>
               sqrt_radical <= std_logic_vector(accx2 + accz2);
--               if(accz >= 0) then
--                  atan2_x   <= std_logic_vector(signed(sqrt_q));
--               else
--                  atan2_x   <= std_logic_vector(0 - signed(sqrt_q));
--               end if;
               atan2_x  <= std_logic_vector(accz);
               atan2_y  <= std_logic_vector(accy);
--               accxang  <= resize(signed(atan2_q&"000000"), accyang'length);
               --accxang <= resize(shift_right(resize(signed(atan2_q&"000000"), 32)*7509888, 16), accxang'length);
               cnt      <= cnt + 1;
               if(cnt > 160) then
                  state <= AccYRadToDeg;
               end if;
               
            when AccYRadToDeg =>
               accyang0  <= accyang;--resize(shift_right(accyang*7509888, 16), accyang0'length);
               accyang1  <= accyang0;
               accyang2  <= accyang1;
               accyang3  <= accyang2;
               accyang4  <= accyang3;
               accyang5  <= accyang4;
               accyang6  <= accyang5;
               accyang7  <= accyang6;
               accyangsum <= accyangsum + accyang - accyang7;
               cnt      <= cnt + 1;
               state    <= AccXRadToDeg;
               
            when AccXRadToDeg =>
               accxang0  <= accxang;--resize(shift_right(accxang*7509888, 16), accxang0'length);
               accxang1  <= accxang0;
               accxang2  <= accxang1;
               accxang3  <= accxang2;
               accxang4  <= accxang3;
               accxang5  <= accxang4;
               accxang6  <= accxang5;
               accxang7  <= accxang6;
               accxangsum <= accxangsum + accxang - accxang7;
               cnt      <= cnt + 1;
               state    <= SensorFusion;
               
            when SensorFusion =>
               angx     <= shift_right(angx + shift_right(accxangsum, 3), 6);
               angy     <= shift_right(angy + shift_right(accyangsum, 3), 6);
               angz     <= angz;
               cnt      <= cnt + 1;
               state    <= SensorXOut;
               
            when SensorXOut =>
               angx_o   <= resize(shift_right(angx*182, 16), angx_o'length);
               cnt      <= cnt + 1;
               state    <= SensorYOut;
               
            when SensorYOut =>
               angy_o   <= resize(shift_right(angy*182, 16), angy_o'length);
               cnt      <= cnt + 1;
               state    <= SensorZOut;
               
            when SensorZOut =>
               angz_o   <= resize(shift_right(angz*182, 16), angz_o'length);
               cnt      <= cnt + 1;
               DRDY     <= '1';
               state    <= wait_for_new_data;
            
            when wait_for_new_data =>
               DRDY     <= '0';
               if(cnt < 6250) then
                  cnt   <= cnt + 1;
               else
                  state <= MPU_read;
               end if;
            
            when others =>
               state          <= who_am_I_ask;
               cnt            <= 0;
               cntoff         <= 0;
               DataOut        <= (others => '0');
               BitLen         <= (others => '0');
               DataLen        <= (others => '0');
               SPIStart       <= '0';
               accx           <= (others => '0');
               accy           <= (others => '0');
               accz           <= (others => '0');
               accx2          <= (others => '0');
               accy2          <= (others => '0');
               accz2          <= (others => '0');
               accxang        <= (others => '0');
               accyang        <= (others => '0');
               accxang0       <= (others => '0');
               accyang0       <= (others => '0');
               accxang1       <= (others => '0');
               accyang1       <= (others => '0');
               accxang2       <= (others => '0');
               accyang2       <= (others => '0');
               accxang3       <= (others => '0');
               accyang3       <= (others => '0');
               accxang4       <= (others => '0');
               accyang4       <= (others => '0');
               accxang5       <= (others => '0');
               accyang5       <= (others => '0');
               accxang6       <= (others => '0');
               accyang6       <= (others => '0');
               accxang7       <= (others => '0');
               accyang7       <= (others => '0');
               accxangsum     <= (others => '0');
               accyangsum     <= (others => '0');
               angx           <= (others => '0');
               angy           <= (others => '0');
               angz           <= (others => '0');
               angx_o         <= (others => '0');
               angy_o         <= (others => '0');
               angz_o         <= (others => '0');
               gyrxoff        <= (others => '0');
               gyryoff        <= (others => '0');
               gyrzoff        <= (others => '0');
               sqrt_radical   <= (others => '0');
               atan2_x        <= (others => '0');
               atan2_y        <= (others => '0');
               DRDY           <= '0';
         end case;
      end if;
   end process;
   
end;