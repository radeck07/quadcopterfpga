## Generated SDC file "ep4cE6_test.out.sdc"

## Copyright (C) 2017  Intel Corporation. All rights reserved.
## Your use of Intel Corporation's design tools, logic functions 
## and other software and tools, and its AMPP partner logic 
## functions, and any output files from any of the foregoing 
## (including device programming or simulation files), and any 
## associated documentation or information are expressly subject 
## to the terms and conditions of the Intel Program License 
## Subscription Agreement, the Intel Quartus Prime License Agreement,
## the Intel MegaCore Function License Agreement, or other 
## applicable license agreement, including, without limitation, 
## that your use is for the sole purpose of programming logic 
## devices manufactured by Intel and sold by Intel or its 
## authorized distributors.  Please refer to the applicable 
## agreement for further details.


## VENDOR  "Altera"
## PROGRAM "Quartus Prime"
## VERSION "Version 17.0.1 Build 598 06/07/2017 SJ Lite Edition"

## DATE    "Tue Jul 25 22:40:43 2017"

##
## DEVICE  "EP4CE6E22C8"
##


#**************************************************************
# Time Information
#**************************************************************

set_time_format -unit ns -decimal_places 3



#**************************************************************
# Create Clock
#**************************************************************

create_clock -name {altera_reserved_tck} -period 100.000 -waveform { 0.000 50.000 } [get_ports {altera_reserved_tck}]
create_clock -name {sys_clk} -period 20.000 -waveform { 0.000 10.000 } [get_ports { sys_clk }]
create_clock -name {clk50M} -period 20.000 -waveform { 0.000 10.000 } [get_pins { PLL_inst|altpll_component|auto_generated|pll1|clk[0] }]
create_clock -name {clk5M} -period 200.000 -waveform { 0.000 100.000 } [get_pins { PLL_inst|altpll_component|auto_generated|pll1|clk[1] }]


#**************************************************************
# Create Generated Clock
#**************************************************************



#**************************************************************
# Set Clock Latency
#**************************************************************



#**************************************************************
# Set Clock Uncertainty
#**************************************************************

set_clock_uncertainty -rise_from [get_clocks {clk50M}] -rise_to [get_clocks {clk50M}]  0.020  
set_clock_uncertainty -rise_from [get_clocks {clk50M}] -fall_to [get_clocks {clk50M}]  0.020  
set_clock_uncertainty -rise_from [get_clocks {clk50M}] -rise_to [get_clocks {clk5M}]  0.020  
set_clock_uncertainty -rise_from [get_clocks {clk50M}] -fall_to [get_clocks {clk5M}]  0.020  
set_clock_uncertainty -fall_from [get_clocks {clk50M}] -rise_to [get_clocks {clk50M}]  0.020  
set_clock_uncertainty -fall_from [get_clocks {clk50M}] -fall_to [get_clocks {clk50M}]  0.020  
set_clock_uncertainty -fall_from [get_clocks {clk50M}] -rise_to [get_clocks {clk5M}]  0.020  
set_clock_uncertainty -fall_from [get_clocks {clk50M}] -fall_to [get_clocks {clk5M}]  0.020  
set_clock_uncertainty -rise_from [get_clocks {sys_clk}] -rise_to [get_clocks {sys_clk}]  0.020  
set_clock_uncertainty -rise_from [get_clocks {sys_clk}] -fall_to [get_clocks {sys_clk}]  0.020  
set_clock_uncertainty -fall_from [get_clocks {sys_clk}] -rise_to [get_clocks {sys_clk}]  0.020  
set_clock_uncertainty -fall_from [get_clocks {sys_clk}] -fall_to [get_clocks {sys_clk}]  0.020  


#**************************************************************
# Set Input Delay
#**************************************************************



#**************************************************************
# Set Output Delay
#**************************************************************



#**************************************************************
# Set Clock Groups
#**************************************************************

set_clock_groups -asynchronous -group [get_clocks {altera_reserved_tck}] 
set_clock_groups -asynchronous -group [get_clocks {altera_reserved_tck}] 
set_clock_groups -asynchronous -group [get_clocks {altera_reserved_tck}] 
set_clock_groups -asynchronous -group [get_clocks {altera_reserved_tck}] 
set_clock_groups -asynchronous -group [get_clocks {altera_reserved_tck}] 
set_clock_groups -asynchronous -group [get_clocks {altera_reserved_tck}] 


#**************************************************************
# Set False Path
#**************************************************************

set_false_path  -from  [get_clocks {clk5M}]  -to  [get_clocks {clk50M}]
set_false_path  -from  [get_clocks {clk50M}]  -to  [get_clocks {clk5M}]
set_false_path -from [get_ports {KEY1 KEY2 RX CH1 CH2 CH3 CH4 sys_clk sys_rst MISO}] 
set_false_path -to [get_ports {TX PPM0 PPM1 PPM2 PPM3 MOSI SCK NCS}]
set_false_path -from [get_keepers {MPU9250Kalman:MPU9250_inst|denom[*]}] -to [get_keepers {MPU9250Kalman:MPU9250_inst|K1_x[*]}]
set_false_path -from [get_keepers {MPU9250Kalman:MPU9250_inst|denom[*]}] -to [get_keepers {MPU9250Kalman:MPU9250_inst|K0_x[*]}]
set_false_path -from [get_keepers {MPU9250Kalman:MPU9250_inst|numer[*]}] -to [get_keepers {MPU9250Kalman:MPU9250_inst|K1_x[*]}]
set_false_path -from [get_keepers {MPU9250Kalman:MPU9250_inst|numer[*]}] -to [get_keepers {MPU9250Kalman:MPU9250_inst|K0_x[*]}]
set_false_path -from [get_keepers {MPU9250Kalman:MPU9250_inst|atan2_x[*]}] -to [get_keepers {MPU9250Kalman:MPU9250_inst|accxang[*]}]
set_false_path -from [get_keepers {MPU9250Kalman:MPU9250_inst|atan2_y[*]}] -to [get_keepers {MPU9250Kalman:MPU9250_inst|accxang[*]}]
set_false_path -from [get_keepers {MPU9250Kalman:MPU9250_inst|atan2_x[*]}] -to [get_keepers {MPU9250Kalman:MPU9250_inst|accyang[*]}]
set_false_path -from [get_keepers {MPU9250Kalman:MPU9250_inst|atan2_y[*]}] -to [get_keepers {MPU9250Kalman:MPU9250_inst|accyang[*]}]


#**************************************************************
# Set Multicycle Path
#**************************************************************



#**************************************************************
# Set Maximum Delay
#**************************************************************



#**************************************************************
# Set Minimum Delay
#**************************************************************



#**************************************************************
# Set Input Transition
#**************************************************************

