library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_misc.all;
use ieee.numeric_std.ALL;
library work;
use work.regs.all; 

entity test is
   PORT(
         sys_clk  :  IN    STD_LOGIC;
         sys_rst  :  IN    STD_LOGIC;
         --KEY1     :  IN    STD_LOGIC;
         --KEY2     :  IN    STD_LOGIC;
         RX       :  IN    STD_LOGIC;
         CH1      :  IN    STD_LOGIC;
         CH2      :  IN    STD_LOGIC;
         CH3      :  IN    STD_LOGIC;
         CH4      :  IN    STD_LOGIC;
         MISO     :  IN    STD_LOGIC;
         TX       :  OUT   STD_LOGIC;
         PPM0     :  OUT   STD_LOGIC;
         PPM1     :  OUT   STD_LOGIC;
         PPM2     :  OUT   STD_LOGIC;
         PPM3     :  OUT   STD_LOGIC;
         MOSI     :  OUT   STD_LOGIC;
         SCK      :  OUT   STD_LOGIC;
         NCS      :  OUT   STD_LOGIC
         
      );
end;

architecture bhv of test is
attribute keep: boolean;
--attribute keep of a: signal is true;
signal clk50M, clk5M, locked, reset       : std_logic;
attribute keep of clk50M: signal is true;
attribute keep of clk5M: signal is true;
--attribute keep of clk10kHz: signal is true;
attribute keep of locked: signal is true;
attribute keep of reset: signal is true;
--signal KEY1_1, KEY1_R, KEY2_1, KEY2_R     : std_logic;
signal rec_AHRSDataRcv                    : record_AHRSDataRcv;
signal rec_SPI0                           : record_SPI;
signal rec_UART_RX1_115200                : record_UART_RX;
signal rec_MPU9250                        : record_MPU9250;
signal rec_AHRSWatchdog                   : record_AHRSWatchdog;
signal rec_UART_TX_115200                 : record_UART_TX;
signal rec_PID                            : record_PID;
signal rec_MotorController                : record_MotorController;
signal rec_BluetoothControl               : record_BluetoothControl;
signal rec_RemoteControlPPM               : record_RemoteControlPPM;
signal rec_YawControl                     : record_YawControl;
signal menabler1                          : std_logic := '0';
signal romtest_a, romtest_b										: std_logic_vector(13 downto 0);
signal sqrt_q : std_logic_vector(47 downto 0);
begin

   TX <= RX;
   
   PLL_inst: ENTITY work.PLL(SYN)
   PORT MAP
   (
      areset   => not sys_rst,
      inclk0   => sys_clk,
      c0       => clk50M,
      c1       => clk5M,
      c2       => open,
      locked   => locked
   );
   
   
   ResetSynchronizer_inst: entity work.ResetSynchronizer(bhv)
   PORT MAP(
      i_clk => clk5M,
      i_rst => (not locked) or (not sys_rst),
      o_rst => reset
   );
   
   
   SPI0_inst: entity work.SPI(bhv)
   PORT MAP(
      i_clk       => rec_SPI0.i_clk,
      i_rst       => rec_SPI0.i_rst,
      i_tick      => rec_SPI0.i_tick,
      iv_DataOut  => rec_SPI0.iv_DataOut,
      iv_BitLen   => rec_SPI0.iv_BitLen,
      iv_DataLen  => rec_SPI0.iv_DataLen,
      i_MISO      => rec_SPI0.i_MISO,
      o_MOSI      => rec_SPI0.o_MOSI,
      o_SCK       => rec_SPI0.o_SCK,
      o_NCS       => rec_SPI0.o_NCS,
      o_DRDY      => rec_SPI0.o_DRDY,
      o_BSY       => rec_SPI0.o_BSY,
      ov_DataIn   => rec_SPI0.ov_DataIn
   );
   
   rec_SPI0.i_clk       <= clk50M;
   rec_SPI0.i_rst       <= reset;
   rec_SPI0.i_MISO      <= MISO;
   MOSI                 <= rec_SPI0.o_MOSI;
   SCK                  <= rec_SPI0.o_SCK;
   NCS                  <= rec_SPI0.o_NCS;
   rec_SPI0.iv_DataOut  <= rec_MPU9250.ov_DataOut;
   rec_SPI0.iv_BitLen   <= rec_MPU9250.ov_BitLen;
   rec_SPI0.iv_DataLen  <= rec_MPU9250.ov_DataLen;
   rec_SPI0.i_tick      <= rec_MPU9250.o_SPIStart;
   
   
   MPU9250_inst: entity work.MPU9250Kalman(bhv)
   PORT MAP(
      i_clk       => rec_MPU9250.i_clk, 
      i_clk5M     => clk5M,
      i_rst       => rec_MPU9250.i_rst,     
      iv_DataIn   => rec_MPU9250.iv_DataIn, 
      i_SPIBsy    => rec_MPU9250.i_SPIBsy,  
      i_SPIDrdy   => rec_MPU9250.i_SPIDrdy, 
      ov_DataOut  => rec_MPU9250.ov_DataOut,
      ov_BitLen   => rec_MPU9250.ov_BitLen, 
      ov_DataLen  => rec_MPU9250.ov_DataLen,
      o_SPIStart  => rec_MPU9250.o_SPIStart,
      ov_angx     => rec_MPU9250.ov_angx,    
      ov_angy     => rec_MPU9250.ov_angy,  
      ov_angz     => rec_MPU9250.ov_angz,
      o_DRDY      => rec_MPU9250.o_DRDY,                                 --Goes low at new valid data recognition
		ov_romtest_a  => romtest_a,
		ov_romtest_b  => romtest_b,
		ov_sqrt_q	=> sqrt_q
   );
   
   rec_MPU9250.i_clk       <= clk50M;
   rec_MPU9250.i_rst       <= reset;
   rec_MPU9250.iv_DataIn   <= rec_SPI0.ov_DataIn;
   rec_MPU9250.i_SPIBsy    <= rec_SPI0.o_BSY;
   rec_MPU9250.i_SPIDrdy   <= rec_SPI0.o_DRDY;
   
   
   UART_RX1_115200_inst: entity work.UART_RX(bhv)
   GENERIC MAP(
      G_BIT_LENGTH   => 433                      --Bit length in clk tacts
   )
   PORT MAP(
      i_clk       => rec_UART_RX1_115200.i_clk,          --input clk
      i_rst       => rec_UART_RX1_115200.i_rst,          --reset signal
      i_RX        => rec_UART_RX1_115200.i_RX,           --data input
      o_DRDY      => rec_UART_RX1_115200.o_DRDY,         --data ready
      ov_RXDATA   => rec_UART_RX1_115200.ov_RXDATA
   );
   
   rec_UART_RX1_115200.i_clk   <= clk50M;
   rec_UART_RX1_115200.i_rst   <= reset;
   rec_UART_RX1_115200.i_RX    <= RX;
   
   
   PID_inst: entity work.PID(bhv)
   PORT MAP(
      i_clk             => rec_PID.i_clk,                      --Clock
      i_rst             => rec_PID.i_rst,                      --Reset
      i_tick            => rec_PID.i_tick,                     --Tick (new calc trigger)
      iv_throttleRC     => rec_PID.iv_throttleRC,              --Throttle value
      iv_yawRC          => rec_PID.iv_yawRC,                   --Yaw setpoint
      iv_pitchRC        => rec_PID.iv_pitchRC,                 --Pitch setpoint
      iv_rollRC         => rec_PID.iv_rollRC,                  --Roll setpoint
      iv_yawAHRS        => rec_PID.iv_yawAHRS,                 --Yaw feedback
      iv_pitchAHRS      => rec_PID.iv_pitchAHRS,               --Pitch feedback
      iv_rollAHRS       => rec_PID.iv_rollAHRS,                --Roll feedback
      iv_yawPGain       => rec_PID.iv_yawPGain,                --Yaw P gain
      iv_yawIGain       => rec_PID.iv_yawIGain,                --Yaw I gain
      iv_yawDGain       => rec_PID.iv_yawDGain,                --Yaw D gain
      iv_pitchPGain     => rec_PID.iv_pitchPGain,              --Pitch P gain
      iv_pitchIGain     => rec_PID.iv_pitchIGain,              --Pitch I gain
      iv_pitchDGain     => rec_PID.iv_pitchDGain,              --Pitch D gain
      iv_rollPGain      => rec_PID.iv_rollPGain,               --Roll P gain
      iv_rollIGain      => rec_PID.iv_rollIGain,               --Roll I gain
      iv_rollDGain      => rec_PID.iv_rollDGain,               --Roll D gain
      iv_yawRatePGain   => rec_PID.iv_yawRatePGain,   
      iv_yawRateIGain   => rec_PID.iv_yawRateIGain,   
      iv_yawRateDGain   => rec_PID.iv_yawRateDGain,   
      iv_pitchRatePGain => rec_PID.iv_pitchRatePGain, 
      iv_pitchRateIGain => rec_PID.iv_pitchRateIGain, 
      iv_pitchRateDGain => rec_PID.iv_pitchRateDGain, 
      iv_rollRatePGain  => rec_PID.iv_rollRatePGain,  
      iv_rollRateIGain  => rec_PID.iv_rollRateIGain,   
      iv_rollRateDGain  => rec_PID.iv_rollRateDGain,  
      ov_motorFL        => rec_PID.ov_motorFL,                 --Output Motor Front Left   
      ov_motorFR        => rec_PID.ov_motorFR,                 --Output Motor Front Right
      ov_motorRL        => rec_PID.ov_motorRL,                 --Output Motor Rear Left
      ov_motorRR        => rec_PID.ov_motorRR                  --Output Motor Rear Right
   );
   
--   rec_PID.i_clk              <= clk50M;
   rec_PID.i_rst              <= reset;
   rec_PID.i_tick             <= rec_MotorController.o_timer_pulse;
   rec_PID.iv_throttleRC      <= rec_RemoteControlPPM.ot_PPMCH(3); --rec_BluetoothControl.ov_throttle;
   rec_PID.iv_yawRC           <= rec_RemoteControlPPM.ot_PPMCH(4) - to_signed(32767, rec_RemoteControlPPM.ot_PPMCH(4)'length); --std_logic_vector(signed(rec_RemoteControlPPM.ov_CH4Val) - to_signed(32767, 32));
   rec_PID.iv_pitchRC         <= rec_RemoteControlPPM.ot_PPMCH(2) - to_signed(32767, rec_RemoteControlPPM.ot_PPMCH(2)'length); --std_logic_vector(signed(rec_BluetoothControl.ov_pitch) - to_signed(65536, 32));
   rec_PID.iv_rollRC          <= rec_RemoteControlPPM.ot_PPMCH(1) - to_signed(32767, rec_RemoteControlPPM.ot_PPMCH(1)'length); --std_logic_vector(signed(rec_BluetoothControl.ov_roll) - to_signed(65536, 32));
   rec_PID.iv_yawAHRS         <= rec_MPU9250.ov_angz;
   rec_PID.iv_pitchAHRS       <= rec_MPU9250.ov_angx;
   rec_PID.iv_rollAHRS        <= rec_MPU9250.ov_angy;
   ------------------------------------------------------------------------------
   rec_PID.iv_yawPGain        <= rec_BluetoothControl.ov_yawkp;
   rec_PID.iv_yawIGain        <= rec_BluetoothControl.ov_yawki;
   rec_PID.iv_yawDGain        <= rec_BluetoothControl.ov_yawkd;
   ------------------------------------------------------------------------------
--   rec_PID.iv_pitchPGain      <= rec_BluetoothControl.ov_pitchkp;
--   rec_PID.iv_pitchIGain      <= rec_BluetoothControl.ov_pitchki;
--   rec_PID.iv_pitchDGain      <= rec_BluetoothControl.ov_pitchkd;
   ------------------------------------------------------------------------------
   rec_PID.iv_rollPGain       <= rec_BluetoothControl.ov_rollkp;
   rec_PID.iv_rollIGain       <= rec_BluetoothControl.ov_rollki;
   rec_PID.iv_rollDGain       <= rec_BluetoothControl.ov_rollkd;
   ------------------------------------------------------------------------------
--   rec_PID.iv_yawRatePGain    <= rec_BluetoothControl.ov_yawkp;
--   rec_PID.iv_yawRateIGain    <= rec_BluetoothControl.ov_yawki;
--   rec_PID.iv_yawRateDGain    <= rec_BluetoothControl.ov_yawkd;
   ------------------------------------------------------------------------------
--   rec_PID.iv_pitchRatePGain  <= rec_BluetoothControl.ov_pitchkp;
--   rec_PID.iv_pitchRateIGain  <= rec_BluetoothControl.ov_pitchki;
--   rec_PID.iv_pitchRateDGain  <= rec_BluetoothControl.ov_pitchkd;
   ------------------------------------------------------------------------------
   rec_PID.iv_rollRatePGain   <= rec_BluetoothControl.ov_pitchkp;
   rec_PID.iv_rollRateIGain   <= rec_BluetoothControl.ov_pitchki;
   rec_PID.iv_rollRateDGain   <= rec_BluetoothControl.ov_pitchkd;
   
   
   MotorController_inst: entity work.MotorController(bhv)
   PORT MAP(
      i_clk          => rec_MotorController.i_clk,         
      i_rst          => rec_MotorController.i_rst,         
      i_enable       => rec_MotorController.i_enable,      
      iv_speedFL     => rec_MotorController.iv_speedFL,     
      iv_speedFR     => rec_MotorController.iv_speedFR,    
      iv_speedRL     => rec_MotorController.iv_speedRL,    
      iv_speedRR     => rec_MotorController.iv_speedRR,    
      o_timer_pulse  => rec_MotorController.o_timer_pulse, 
      o_PPMFL        => rec_MotorController.o_PPMFL,       
      o_PPMFR        => rec_MotorController.o_PPMFR,       
      o_PPMRL        => rec_MotorController.o_PPMRL,       
      o_PPMRR        => rec_MotorController.o_PPMRR       
   );
   
--   rec_MotorController.i_clk         <= clk50M;
   rec_MotorController.i_rst         <= reset;
   rec_MotorController.i_enable      <= menabler1 AND rec_BluetoothControl.o_enable;
   rec_MotorController.iv_speedFL    <= rec_PID.ov_motorFL;
   rec_MotorController.iv_speedFR    <= rec_PID.ov_motorFR;
   rec_MotorController.iv_speedRL    <= rec_PID.ov_motorRL;
   rec_MotorController.iv_speedRR    <= rec_PID.ov_motorRR;
   PPM0                              <= rec_MotorController.o_PPMFL; 
   PPM1                              <= rec_MotorController.o_PPMFR;
   PPM2                              <= rec_MotorController.o_PPMRL;
   PPM3                              <= rec_MotorController.o_PPMRR;
   
   
   BluetoothControl_inst: entity work.BluetoothControl(bhv)
   PORT MAP(
      i_clk       => rec_BluetoothControl.i_clk,
      i_rst       => rec_BluetoothControl.i_rst,
      i_tick      => rec_BluetoothControl.i_tick,
      iv_RXdata   => rec_BluetoothControl.iv_RXdata,
      ov_throttle => rec_BluetoothControl.ov_throttle,
      ov_yaw      => rec_BluetoothControl.ov_yaw,
      ov_pitch    => rec_BluetoothControl.ov_pitch,
      ov_roll     => rec_BluetoothControl.ov_roll,
      ov_yawkp    => rec_BluetoothControl.ov_yawkp,
      ov_yawki    => rec_BluetoothControl.ov_yawki,
      ov_yawkd    => rec_BluetoothControl.ov_yawkd,
      ov_pitchkp  => rec_BluetoothControl.ov_pitchkp,
      ov_pitchki  => rec_BluetoothControl.ov_pitchki,
      ov_pitchkd  => rec_BluetoothControl.ov_pitchkd,
      ov_rollkp   => rec_BluetoothControl.ov_rollkp,
      ov_rollki   => rec_BluetoothControl.ov_rollki,
      ov_rollkd   => rec_BluetoothControl.ov_rollkd,
      o_enable    => rec_BluetoothControl.o_enable
   );
      
--   rec_BluetoothControl.i_clk       <= clk50M;
   rec_BluetoothControl.i_rst       <= reset;   
   rec_BluetoothControl.i_tick      <= rec_UART_RX1_115200.o_DRDY;   
   rec_BluetoothControl.iv_RXdata   <= rec_UART_RX1_115200.ov_RXDATA;
   
   
   RemoteControlPPM_inst: entity work.RemoteControlPPM(bhv)
   GENERIC MAP(
      G_CHANNELS_N   => 4
   )
   PORT MAP(
      i_clk          => rec_RemoteControlPPM.i_clk,    
      i_rst          => rec_RemoteControlPPM.i_rst,   
      i_enable       => rec_RemoteControlPPM.i_enable, 
      iv_CHPPM       => rec_RemoteControlPPM.iv_CHPPM, 
      ot_PPMCH       => rec_RemoteControlPPM.ot_PPMCH
   );
   
--   rec_RemoteControlPPM.i_clk    <= clk50M; 
   rec_RemoteControlPPM.i_rst    <= reset;
   rec_RemoteControlPPM.i_enable <= '1';
   rec_RemoteControlPPM.iv_CHPPM(1) <= CH1;
   rec_RemoteControlPPM.iv_CHPPM(2) <= CH2;
   rec_RemoteControlPPM.iv_CHPPM(3) <= CH3;
   rec_RemoteControlPPM.iv_CHPPM(4) <= CH4;
   
   
   process(clk50M)
   begin
      if(rising_edge(clk50M)) then
--         KEY1_1   <= KEY1;
--         KEY1_R   <= KEY1_1;
--         
--         KEY2_1   <= KEY2;
--         KEY2_R   <= KEY2_1;
         
         if(rec_MPU9250.o_DRDY = '1') then
            menabler1 <= '1';
         end if;
         
         
      end if;
   end process;
   
end bhv;