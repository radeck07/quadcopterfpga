-------------------------------------------
-- File:   sqrt.vhd
--
-- Author: Mike Field <hamster@snap.net.nz>
--
-- Calculate the square root of an integer
-- in length/2+1 cycles. (e.g a 31 or 32 bit
-- integer takes 16 cycles to find the result)
--
-- It relys on the fact that setting bit 'n' in
-- a partial result 'p' will increase the value
-- of p*p by 2^n*2^n+p*2^n+p*2^n. This can all
-- be calculated using bit shifts (which are
-- free in FPGAs
-------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library work;

entity sqrt_2 is
    Port ( clk : in  STD_LOGIC;
           number : in  STD_LOGIC_VECTOR;
           result : out  STD_LOGIC_VECTOR);
end sqrt_2;

architecture Behavioral of sqrt_2 is
   type a_partial   is array(result'high downto 0) of unsigned(result'range);
   type a_remaining is array(result'high downto 0) of unsigned(number'range);
   signal partial   : a_partial   := (others => (others => '0'));
   signal remaining : a_remaining := (others => (others => '0'));
   signal zeros : unsigned (number'high downto 0) := (others => '0');
begin
   -----------------------------------------------
   -- Calculate the square root, one bit per cycle.
   -----------------------------------------------
calc_proc: process(clk)
   variable change : unsigned(number'range);
begin
   if rising_edge(clk) then

      -------------------------------------------------------------------------------------
      -- Extracting the topmost bit of the result is different, as we have to allow
      -- for an odd number of bits in "number".
      --
      -- If 'number' has an odd number of bits, then is the MSB is set
      -- If 'number' has an even number of bits, are any of the top bits are set
      --
      -- If so, set the highest bit in result.
      -------------------------------------------------------------------------------------
      if unsigned(number(number'high downto (number'high/2)*2)) > 0 then
         -- Calculate the first partial result, by copying everything over
         remaining(number'high/2) <= unsigned(number);
         -- then subtracting 1 from the highest bit/bits
         remaining(number'high/2)(number'high downto (number'high/2)*2) <= unsigned(number(number'high downto (number'high/2)*2))-1;        
         partial(partial'high)     <= (number'high/2 => '1', others => '0');
      else
         -- Top bit of partial result is not set
         remaining(remaining'high) <= unsigned(number);
         partial(partial'high)     <= (others => '0');
      end if;

      ------------------------------------------------
      -- Now calculate all the other bits the hard way
      ------------------------------------------------
      for i in number'high/2-1 downto 0 loop
         change := zeros + ("1" & zeros(i-1 downto 0) & zeros(i-1 downto 0)) + (partial(i+1) & zeros(i downto 0));
         if remaining(i+1) >= change then
            remaining(i)  <= remaining(i+1) - change;
            partial(i)    <= partial(i+1);
            partial(i)(i) <= '1';
         else
            remaining(i) <= remaining(i+1);
            partial(i)   <= partial(i+1);
         end if;
      end loop;

      ----------------------------------------
      -- Finally zero out the bits that 
      -- we know will be zero, to give the 
      -- the logic optimizer hints.
      ----------------------------------
      for i in number'high/2-1 downto 0 loop
         remaining(i)(number'high downto i+(number'high)/2+2) <= (others => '0');
      end loop;
   end if;

end process;

-----------------------------------------------
-- assign the output to the final result
-----------------------------------------------
result <= std_logic_vector(partial(0));

end Behavioral;