--Name:     PID
--Author:   Radoslaw Kalinka
--Date:     2016.10.17
--Desc:     PID module 
--          Module works with 50MHz input clock
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_misc.all;
use ieee.numeric_std.ALL;
library work;
use work.regs.all; 

entity PID is
   PORT(
      i_clk                : IN std_logic;--Clock
      i_rst                : IN std_logic;--Reset
      i_tick               : IN std_logic;--Tick (new calc trigger)
      iv_throttleRC        : IN s2q16;    --Throttle value
      iv_yawRC             : IN s2q16;    --Yaw setpoint
      iv_pitchRC           : IN s2q16;    --Pitch setpoint
      iv_rollRC            : IN s2q16;    --Roll setpoint
      iv_yawAHRS           : IN s2q16;    --Yaw feedback
      iv_pitchAHRS         : IN s2q16;    --Pitch feedback
      iv_rollAHRS          : IN s2q16;    --Roll feedback
      iv_yawPGain          : IN s6q16;    --Yaw P gain
      iv_yawIGain          : IN s6q16;    --Yaw I gain
      iv_yawDGain          : IN s6q16;    --Yaw D gain
      iv_pitchPGain        : IN s6q16;    --Pitch P gain
      iv_pitchIGain        : IN s6q16;    --Pitch I gain
      iv_pitchDGain        : IN s6q16;    --Pitch D gain
      iv_rollPGain         : IN s6q16;    --Roll P gain
      iv_rollIGain         : IN s6q16;    --Roll I gain
      iv_rollDGain         : IN s6q16;    --Roll D gain
      iv_yawRatePGain      : IN s6q16;    --Yaw Rate P gain
      iv_yawRateIGain      : IN s6q16;    --Yaw Rate I gain
      iv_yawRateDGain      : IN s6q16;    --Yaw Rate D gain
      iv_pitchRatePGain    : IN s6q16;    --Pitch Rate P gain
      iv_pitchRateIGain    : IN s6q16;    --Pitch Rate I gain
      iv_pitchRateDGain    : IN s6q16;    --Pitch Rate D gain
      iv_rollRatePGain     : IN s6q16;    --Roll Rate P gain
      iv_rollRateIGain     : IN s6q16;    --Roll Rate I gain
      iv_rollRateDGain     : IN s6q16;    --Roll Rate D gain
      ov_motorFL           : OUT s2q16;   --Output Motor Front Left   
      ov_motorFR           : OUT s2q16;   --Output Motor Front Right
      ov_motorRL           : OUT s2q16;   --Output Motor Rear Left
      ov_motorRR           : OUT s2q16    --Output Motor Rear Right
   );
end;

architecture bhv of PID is
   
   signal yawAHRS_old, pitchAHRS_old, rollAHRS_old : s2q16;
   signal PID_yaw, PID_pitch, PID_roll             : s2q16;
   signal Pp_yaw, Pp_pitch, Pp_roll                : s3q16   := (others => '0');
   signal I_yaw, I_pitch, I_roll                   : s2q16   := (others => '0');
   signal PID_yawRate, PID_pitchRate, PID_rollRate : s2q16;
   signal Pp_yawRate, Pp_pitchRate, Pp_rollRate    : s3q16   := (others => '0');
   signal I_yawRate, I_pitchRate, I_rollRate       : s2q16   := (others => '0');
   signal motorFLv, motorFRv, motorRLv, motorRRv   : s4q16;
   signal motorFL, motorFR, motorRL, motorRR       : s2q16 := (others => '0');
   signal highest                                  : s4q16;
   
   type StateType is (PID_start, PID_yaw_calc, PID_yaw_get, PID_pitch_calc, PID_pitch_get, PID_roll_calc, PID_roll_get, PID_yawRate_calc, PID_yawRate_get, PID_pitchRate_calc, PID_pitchRate_get, PID_rollRate_calc, PID_rollRate_get, PID_add_all, motorNorm1, motorNorm2, motorNorm3, motorNorm4, motorZero, motorUpdate); --State machine states typedef
   signal state : StateType := PID_start;
   
   signal set, fdb : s2q16;
   signal Ppin, Ppout : s3q16;
   signal Iin  : s2q16;
   signal Iout, P, D, PID : s16q16;
   signal Kp, Ki, Kd   : s6q16;
   type CalcPIDStateType is (CalcP, CalcD, CalcI, CalcILimit, CalcPIDSum, CalcPIDLimit, CalcPIDrdy);
   signal CalcPIDstate : CalcPIDStateType := CalcP;
   signal PIDstart, PIDready : std_logic;
   
begin
   ov_motorFL <= motorFL;
   ov_motorFR <= motorFR;
   ov_motorRL <= motorRL;
   ov_motorRR <= motorRR;

   state_machine:process(i_clk, i_rst) is
   begin
      if(i_rst = '1') then
         state          <= PID_start;
         yawAHRS_old    <= (others => '0');
         pitchAHRS_old  <= (others => '0');
         rollAHRS_old   <= (others => '0');
         I_yaw          <= (others => '0');
         I_pitch        <= (others => '0');
         I_roll         <= (others => '0');
         motorFL        <= (others => '0');
         motorFR        <= (others => '0');
         motorRL        <= (others => '0');
         motorRR        <= (others => '0');
      elsif(rising_edge(i_clk)) then
         case state is
            when PID_start =>
               PIDstart <= '0';
               if(i_tick = '1') then
                  state <= PID_yaw_calc;
               end if;
               
            --ANGULAR PIDs-----------------------------------------------------------   
            when PID_yaw_calc   =>
               Iin         <= I_yaw;
               Ppin        <= Pp_yaw;
               set         <= resize(iv_yawRC, set'length);
               fdb         <= resize(iv_yawAHRS, fdb'length);
               Kp          <= iv_yawPGain;
               Ki          <= iv_yawIGain;
               Kd          <= iv_yawDGain;
               PIDstart    <= '1';
               if(PIDready = '0') then
                  state    <= PID_yaw_get;
               end if;
               
            when PID_yaw_get   =>
               I_yaw       <= resize(Iout, I_yaw'length);
               Pp_yaw      <= Ppout;
               PID_yaw     <= resize(PID, PID_yaw'length);
               PIDstart    <= '0';
               if(PIDready = '1') then
                  state    <= PID_pitch_calc;
               end if;
               
            when PID_pitch_calc   =>
               Iin         <= I_pitch;
               Ppin        <= Pp_pitch;
               set         <= resize(iv_pitchRC, set'length);
               fdb         <= resize(iv_pitchAHRS, fdb'length);
               Kp          <= iv_pitchPGain;
               Ki          <= iv_pitchIGain;
               Kd          <= iv_pitchDGain;
               PIDstart    <= '1';
               if(PIDready = '0') then
                  state    <= PID_pitch_get;
               end if;
               
            when PID_pitch_get   =>
               I_pitch     <= resize(Iout, I_pitch'length);
               Pp_pitch    <= Ppout;
               PID_pitch   <= resize(PID, PID_pitch'length);
               PIDstart    <= '0';
               if(PIDready = '1') then
                  state    <= PID_roll_calc;
               end if;
               
            when PID_roll_calc   =>
               Iin         <= I_roll;
               Ppin        <= Pp_roll;
               set         <= resize(iv_rollRC, set'length);
               fdb         <= resize(iv_rollAHRS, fdb'length);
               Kp          <= iv_rollPGain;
               Ki          <= iv_rollIGain;
               Kd          <= iv_rollDGain;
               PIDstart    <= '1';
               if(PIDready = '0') then
                  state    <= PID_roll_get;
               end if;
               
            when PID_roll_get   =>
               I_roll      <= resize(Iout, I_roll'length);
               Pp_roll     <= Ppout;
               PID_roll    <= resize(PID, PID_roll'length);
               PIDstart    <= '0';
               if(PIDready = '1') then
                  state    <= PID_yawRate_calc;
               end if;
            --RATE PIDs--------------------------------------------------------------
            when PID_yawRate_calc   =>
               Iin         <= I_yawRate;
               Ppin        <= Pp_yawRate;
               --set         <= resize(PID_yaw, set'length);
               set         <= resize(iv_yawRC, set'length);
               fdb         <= resize(iv_yawAHRS - yawAHRS_old, fdb'length);
               yawAHRS_old <= iv_yawAHRS;
               Kp          <= iv_yawRatePGain;
               Ki          <= iv_yawRateIGain;
               Kd          <= iv_yawRateDGain;
               PIDstart    <= '1';
               if(PIDready = '0') then
                  state    <= PID_yawRate_get;
               end if;
               
            when PID_yawRate_get   =>
               I_yawRate   <= resize(Iout, I_yawRate'length);
               Pp_yawRate  <= Ppout;
               PID_yawRate <= resize(PID, PID_yawRate'length);
               PIDstart    <= '0';
               if(PIDready = '1') then
                  state    <= PID_pitchRate_calc;
               end if;
               
            when PID_pitchRate_calc   =>
               Iin         <= I_pitchRate;
               Ppin        <= Pp_pitchRate;
               --set         <= resize(PID_pitch, set'length);
               set         <= resize(iv_pitchRC, set'length);
               fdb         <= resize(iv_pitchAHRS - pitchAHRS_old, fdb'length);
               pitchAHRS_old <= iv_pitchAHRS;
               Kp          <= iv_pitchRatePGain;
               Ki          <= iv_pitchRateIGain;
               Kd          <= iv_pitchRateDGain;
               PIDstart    <= '1';
               if(PIDready = '0') then
                  state    <= PID_pitchRate_get;
               end if;
               
            when PID_pitchRate_get   =>
               I_pitchRate    <= resize(Iout, I_pitchRate'length);
               Pp_pitchRate   <= Ppout;
               PID_pitchRate  <= resize(PID, PID_pitchRate'length);
               PIDstart       <= '0';
               if(PIDready = '1') then
                  state       <= PID_rollRate_calc;
               end if;
               
            when PID_rollRate_calc   =>
               Iin            <= I_rollRate;
               Ppin           <= Pp_rollRate;
               set            <= resize(PID_roll, set'length);
--               set            <= resize(iv_rollRC, set'length);
               fdb            <= resize(iv_rollAHRS - rollAHRS_old, fdb'length);
               rollAHRS_old   <= iv_rollAHRS;
               Kp             <= iv_rollRatePGain;
               Ki             <= iv_rollRateIGain;
               Kd             <= iv_rollRateDGain;
               PIDstart       <= '1';
               if(PIDready = '0') then
                  state       <= PID_rollRate_get;
               end if;
               
            when PID_rollRate_get   =>
               I_rollRate     <= resize(Iout, I_rollRate'length);
               Pp_rollRate    <= Ppout;
               PID_rollRate   <= resize(PID, PID_rollRate'length);
               PIDstart       <= '0';
               if(PIDready = '1') then
                  state       <= PID_add_all;
               end if;
               
            ----------------------------------------------------------------------------
               
            when PID_add_all =>
               motorFLv <= resize(iv_throttleRC, motorFLv'length) - PID_yawRate + PID_pitchRate + PID_rollRate;
               motorFRv <= resize(iv_throttleRC, motorFRv'length) + PID_yawRate + PID_pitchRate - PID_rollRate;
               motorRLv <= resize(iv_throttleRC, motorRLv'length) + PID_yawRate - PID_pitchRate + PID_rollRate;
               motorRRv <= resize(iv_throttleRC, motorRRv'length) - PID_yawRate - PID_pitchRate - PID_rollRate;
               
               state <= motorNorm1;
               
            ----------------------------------------------------------------------------  
               
            when motorNorm1 =>
               if(motorFRv > motorFLv) then
                  highest <= motorFRv;
               else
                  highest <= motorFLv;
               end if;
               
               state <= motorNorm2;
            
            when motorNorm2 =>
               if(motorRLv > highest) then
                  highest  <= motorRLv;
               end if;
               
               state <= motorNorm3;
               
            when motorNorm3 =>
               if(motorRRv > highest) then
                  highest  <= motorRRv;
               end if;
               
               state <= motorNorm4;
               
            when motorNorm4 =>
               if(highest > 65535) then
                  motorFLv <= motorFLv - highest + to_signed(65535, motorFLv'length);
                  motorFRv <= motorFRv - highest + to_signed(65535, motorFRv'length);
                  motorRRv <= motorRRv - highest + to_signed(65535, motorRRv'length);
                  motorRLv <= motorRLv - highest + to_signed(65535, motorRLv'length);
               end if;
               
               state <= motorZero;
               
            ----------------------------------------------------------------------------
            
            when motorZero =>
               if(motorFLv < 0) then
                  motorFLv <= (others => '0');
               end if;
               
               if(motorFRv < 0) then
                  motorFRv <= (others => '0');
               end if;
               
               if(motorRLv < 0) then
                  motorRLv <= (others => '0');
               end if;
               
               if(motorRRv < 0) then
                  motorRRv <= (others => '0');
               end if;
               
               state <= motorUpdate;
               
            ----------------------------------------------------------------------------
            
            when motorUpdate =>
               motorFL  <= resize(motorFLv, motorFL'length);
               motorFR  <= resize(motorFRv, motorFR'length);
               motorRL  <= resize(motorRLv, motorRL'length);
               motorRR  <= resize(motorRRv, motorRR'length);
               
               state    <= PID_start;
               
            when others =>
               state    <= PID_start;
               I_yaw    <= (others => '0');
               I_pitch  <= (others => '0');
               I_roll   <= (others => '0');
               motorFL  <= (others => '0');
               motorFR  <= (others => '0');
               motorRL  <= (others => '0');
               motorRR  <= (others => '0');
               
         end case;
      end if;
   end process state_machine;
   
   process(i_clk, i_rst)
   begin
      if(i_rst = '1') then
         CalcPIDstate <= CalcP;
         P     <= (others => '0');
         D     <= (others => '0');
         PIDready   <= '0';
      elsif(rising_edge(i_clk)) then
         case CalcPIDstate is
            when CalcP =>                                                                                      --Proportional Module
               PIDready <= '1';                                                                                --PID ready for new calculation
               P        <= resize(shift_right((set - fdb) * Kp, 16), P'length);                                --Error * Kp
               if(PIDstart = '1') then                                                                         --Wait for trigger
                  Ppout    <= resize(set, Ppout'length) - resize(fdb, Ppout'length);                           --Last error = set - fdb
                  PIDready <= '0';                                                                             --PID busy
                  CalcPIDstate <= CalcD;                                                                       --Go for differential
               end if;
               
            when CalcD =>                                                                                      --Differential Module                                                                                    --Store error for future iterations
               D        <= resize(shift_right((Ppout - Ppin) * Kd, 16), D'length);                             --deltaError * Kd
               CalcPIDstate <= CalcI;                                                                          --Go for integral
               
            when CalcI =>                                                                                      --Integral Module
               Iout     <= resize(Iin, Iout'length)  + resize(shift_right((Ppout * Ki), 16), Iout'length);     --Accummulate Error*Ki
               CalcPIDstate <= CalcILimit;                                                                     --Go for integral limit
               
            when CalcILimit =>                                                                                 --Integral limit
               if(Ki > 0) then
                  if(Iout > 65536) then                                                                           --Integral is greater than max integral val
                     Iout <= to_signed(65536, Iout'length);                                                       --Limit integral to max val
                  elsif(Iout < -65536) then                                                                       --Integral is less than min integral val
                     Iout <= to_signed(-65536, Iout'length);                                                      --Limit integral to min val
                  end if;
               else
                  Iout  <= (others => '0');
               end if;
               CalcPIDstate <= CalcPIDSum;                                                                     --Go for PID sum
               
            when CalcPIDSum =>                                                                                 --PID sum
               PID   <= P + D + Iout;                                                                          --PID = proportional + derivative + integral
               CalcPIDstate <= CalcPIDLimit;                                                                   --Go for PID limit
               
            when CalcPIDLimit =>                                                                               --PID limit
               if(PID > 65536) then                                                                            --PID output is above 1.0
                  PID   <= to_signed(65536, PID'length);                                                       --Limit output to 1.0
                  if(to_signed(65536, PID'length) - P - D > 0) then                                            --Integral Antiwindup
                     Iout  <= to_signed(65536, Iout'length) - resize(P, Iout'length) - resize(D, Iout'length); --Cut integral excess
                  else
                     Iout  <= (others => '0');
                  end if;
               elsif(PID < -65536) then                                                                        --PID output is below -1.0
                  PID   <= to_signed(-65536, PID'length);                                                      --Limit output to -1.0
                  if(to_signed(-65536, PID'length) - P - D < 0) then
                     Iout  <= to_signed(-65536, Iout'length) - resize(P, Iout'length) - resize(D, Iout'length);
                  else
                     Iout  <= (others => '0');
                  end if;
               end if;
               CalcPIDstate <= CalcPIDrdy;                                                                     --Go for PID ready
               
            when CalcPIDrdy =>                                                                                 --PID ready
               if(PIDstart = '0') then                                                                         --Wait for trigger release
                  CalcPIDstate   <= CalcP;                                                                     --Go for next proportional calc
                  PIDready       <= '1';                                                                       --PID ready
               end if;
            
            when others =>
               P     <= (others => '0');
               D     <= (others => '0');
               PIDready   <= '0';
               
         end case;
      end if;
   end process;
--   
end;

