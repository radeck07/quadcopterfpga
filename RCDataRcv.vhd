library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.ALL;

entity RCDataRcv is
	PORT(
			clk		:	IN STD_LOGIC;
			tick		:	IN	STD_LOGIC;
			RXdata	:	IN	STD_LOGIC_vector(7 downto 0);
			throttle	:	OUT std_logic_vector(9 downto 0):=(OTHERS => '0');
			yaw		:	OUT std_logic_vector(9 downto 0):=(OTHERS => '0');
			pitch		:	OUT std_logic_vector(9 downto 0):=(OTHERS => '0');
			roll		:	OUT std_logic_vector(9 downto 0):=(OTHERS => '0');	
			DRDY		:	OUT std_logic:='1'											--Goes low at new valid data recognition
		);
end;

architecture bhv of RCDataRcv is

	type StateType is (th, tl, yh, yl, ph, pl, rh, rl, chksum);	--State machine states typedef
	signal state : StateType; 												--State machine declaration
	signal T, Y, P, R : std_logic_vector(9 downto 0);				--Temporary value signals (for checksum)
	
begin
	state_machine: process(RXdata, state, clk)
	begin
		if falling_edge(clk) then
			DRDY <= '1';
			if (tick = '0') then
				case state is
					--Throttle higher bits--
					when th =>
						DRDY <= '1';
						if(RXdata(7 downto 5) = "000") then
							state <= tl;
							T(9 downto 5) <= RXdata(4 downto 0);
						else
							state <= th;
						end if;
					------------------------
					
					--Throttle lower bits--
					when tl =>
						if(RXdata(7 downto 5) = "001") then
							state <= yh;
							T(4 downto 0) <= RXdata(4 downto 0);
						else
							state <= th;
						end if;
					-----------------------
					
					--Yaw higher bits--
					when yh =>
						if(RXdata(7 downto 5) = "010") then
							state <= yl;
							Y(9 downto 5) <= RXdata(4 downto 0);
						else
							state <= th;
						end if;
					-------------------
					
					--Yaw lower bits--
					when yl =>
						if(RXdata(7 downto 5) = "011") then
							state <= ph;
							Y(4 downto 0) <= RXdata(4 downto 0);
						else
							state <= th;
						end if;
					------------------
					
					--Pitch higher bits--
					when ph =>
						if(RXdata(7 downto 5) = "100") then
							state <= pl;
							P(9 downto 5) <= RXdata(4 downto 0);
						else
							state <= th;
						end if;
					---------------------
					
					--Pitch lower bits--
					when pl =>
						if(RXdata(7 downto 5) = "101") then
							state <= rh;
							P(4 downto 0) <= RXdata(4 downto 0);
						else
							state <= th;
						end if;
					--------------------
					
					--Roll higher bits--
					when rh =>
						if(RXdata(7 downto 5) = "110") then
							state <= rl;
							R(9 downto 5) <= RXdata(4 downto 0);
						else
							state <= th;
						end if;
					--------------------
					
					--Roll lower bits--
					when rl =>
						if(RXdata(7 downto 5) = "111") then
							state <= chksum;
							R(4 downto 0) <= RXdata(4 downto 0);
						else
							state <= th;
						end if;
					-------------------
					
					--CHECKSUM--
					when chksum =>
						state <= th;
						throttle <= T;
						yaw <= Y;
						pitch <= P;
						roll <= R;
						DRDY <= '0';
				end case;
			end if;	
		end if;
	end process state_machine;
	
	 
	
end bhv;