--Name:     Divider_TB
--Author:   Radoslaw Kalinka
--Date:     2017.10.28
--Desc:     Divider module testbench
--          Module works with 50MHz input clock
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.ALL; 

entity Divider_TB is
end;

architecture bhv of Divider_TB is
component Divider is
   PORT(
      i_clk       : IN STD_LOGIC;
      i_rst       : IN STD_LOGIC;
      i_tick      : IN std_logic;                     --spi send request
      iv_Numer    : IN std_logic_vector; --data to send
	  iv_Denomin  : IN std_logic_vector; --data to send
	  ov_Result   : OUT std_logic_vector; --data to send
      o_DRDY      : OUT std_logic;
      o_BSY       : OUT std_logic
   );
end component;
   
   signal clk         : std_logic;                  
   signal rst         : std_logic;                  
   signal tick      : std_logic;
   signal Numer    : std_logic_vector(47 downto 0);
   signal Denomin  : std_logic_vector(15 downto 0);
   signal Result   : std_logic_vector(47 downto 0);
   signal DRDY      : std_logic;
   signal BSY       : std_logic;
   
   
begin
                           
   Divider_inst: Divider port map (
                           i_clk       => clk,
                           i_rst       => rst,
                           i_tick      => tick,
                           iv_Numer    => Numer,
                           iv_Denomin  => Denomin,
                           ov_Result   => Result,
                           o_DRDY      => DRDY,
                           o_BSY       => BSY
                         );  
   
   
   clk_gen: process                
   begin                           
      clk <= '0';                 
      wait for 10 ns;             
      clk <= '1';                 
      wait for 10 ns;
   end process;
   
   stim_process: process
   begin
      Numer <= std_logic_vector(to_signed(131072, Numer'length));
	  Denomin <= std_logic_vector(to_signed(1024, Denomin'length));
      rst <= '1';
      tick <= '0';
      wait for 20 ns;
      rst <= '0';
      wait for 20 ns;
      tick  <= '1';
      wait for 20 ns;
      tick  <= '0';
	  
      wait;
   end process;
   
end bhv;

