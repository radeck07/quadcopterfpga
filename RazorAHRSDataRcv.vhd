--Name:     RazorAHRSDataRcv
--Author:   Radoslaw Kalinka
--Date:     2017.05.31
--Desc:     Razor AHRS Data Receiver module 
--          Module works with 50MHz input clock
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_misc.all;
use ieee.numeric_std.ALL;
library work;
use work.regs.all; 

entity RazorAHRSDataRcv is
   PORT(
         i_clk      :  IN STD_LOGIC;
         i_rst      :  IN STD_LOGIC;
         i_tick     :  IN STD_LOGIC;
         iv_RXdata  :  IN byte;
         ov_yaw     :  OUT s2q16;
         ov_pitch   :  OUT s2q16;
         ov_roll    :  OUT s2q16; 
         o_DRDY     :  OUT std_logic                               --Goes low at new valid data recognition
      );
end;

architecture bhv of RazorAHRSDataRcv is
   type StateType is (hash, Y, P, R, Eq_sign, parse_yaw, parse_pitch, parse_roll, update);   --State machine states typedef
   signal state : StateType := hash;                                    --State machine declaration
   signal vR, vP, vY          : s2q16;
   signal yaw, pitch, roll    : s2q16;
   signal DRDY                : std_logic := '0';
   signal minus               : std_logic;
   signal container           : s2q16;
   constant wsp                 : signed(18 downto 0) := to_signed(238609, 19);
begin
   o_DRDY <= DRDY;
   ov_yaw   <= yaw;
   ov_pitch <= pitch;
   ov_roll  <= roll;
   
   process(i_rst, i_clk)
   begin
      if(i_rst = '1') then
         state <= hash;
         yaw   <= (others => '0');
         pitch <= (others => '0');
         roll  <= (others => '0');
         minus <= '0';
      elsif(rising_edge(i_clk)) then
         if(i_tick = '1') then
            case state is
               when hash =>
                  if(iv_RXdata = "00100011") then
                     state <= Y;
                  end if;
                  DRDY <= '0';
                  
               when Y =>
                  if(iv_RXdata = "01011001") then
                     state <= P;
                  else
                     state <= hash;
                  end if;
               
               when P =>
                  if(iv_RXdata = "01010000") then
                     state <= R;
                  else
                     state <= hash;
                  end if;
               
               when R =>
                  if(iv_RXdata = "01010010") then
                     state <= Eq_sign;
                  else
                     state <= hash;
                  end if;
               
               when Eq_sign =>
                  if(iv_RXdata = "00111101") then
                     state <= parse_yaw;
                  else
                     state <= hash;
                  end if;
                  minus       <= '0';
                  container   <= (others => '0');
               
               when parse_yaw =>
                  if(iv_RXdata = "00101101") then -- -
                     minus <= '1';
                  elsif( unsigned(iv_RXdata) > 47 AND unsigned(iv_RXdata) < 58 ) then
                     container <= resize(container * 10, container'length) + resize(signed('0'&iv_RXdata(3 downto 0)), container'length);
                  elsif(iv_RXdata = "00101100") then -- ,
                     if(minus = '1') then
                        yaw <= to_signed(0, yaw'length) - resize(shift_right(container * wsp, 16), yaw'length);
                     else
                        yaw <= resize(shift_right(container * wsp, 16), yaw'length);
                     end if;
                     container   <= (others => '0');
                     minus       <= '0';
                     state       <= parse_pitch;
                  elsif(iv_RXdata /= "00101110") then -- .
                     state <= hash;
                  end if;
               
               when parse_pitch =>
                  if(iv_RXdata = "00101101") then -- -
                     minus <= '1';
                  elsif( unsigned(iv_RXdata) > 47 AND unsigned(iv_RXdata) < 58 ) then
                     container <= resize(container * 10, container'length) + resize(signed('0'&iv_RXdata(3 downto 0)), container'length);
                  elsif(iv_RXdata = "00101100") then -- ,
                     if(minus = '1') then
                        pitch <= to_signed(0, pitch'length) - resize(shift_right(container * wsp, 16), pitch'length);
                     else
                        pitch <= resize(shift_right(container * wsp, 16), pitch'length);
                     end if;
                     container   <= (others => '0');
                     minus       <= '0';
                     state       <= parse_roll;
                  elsif(iv_RXdata /= "00101110") then -- .
                     state <= hash;
                  end if;
               
               when parse_roll =>
                  if(iv_RXdata = "00101101") then -- -
                     minus <= '1';
                  elsif( unsigned(iv_RXdata) > 47 AND unsigned(iv_RXdata) < 58 ) then
                     container <= resize(container * 10, container'length) + resize(signed('0'&iv_RXdata(3 downto 0)), container'length);
                  elsif(iv_RXdata = "00001101") then -- 0D
                     if(minus = '1') then
                        roll <= to_signed(0, roll'length) - resize(shift_right(container * wsp, 16), roll'length);
                     else
                        roll <= resize(shift_right(container * wsp, 16), roll'length);
                     end if;
                     container   <= (others => '0');
                     minus       <= '0';
                     state       <= update;
                  elsif(iv_RXdata /= "00101110") then -- .
                     state <= hash;
                  end if;
               
               when update =>
                  DRDY  <= '1';
                  state <= hash;
               
               when others =>
               
            end case;
         end if;
      end if;
   end process;
   
end bhv;