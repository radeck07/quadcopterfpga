--Name:     BluetoothControl
--Author:   Radoslaw Kalinka
--Date:     2017.03.26
--Desc:     Bluetooth Remote COntrol Module 
--          Module works with 50MHz input clock
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_misc.all;
use ieee.numeric_std.ALL;
library work;
use work.regs.all; 

entity BluetoothControl is
   PORT(
         i_clk       :  IN STD_LOGIC;
         i_rst       :  IN STD_LOGIC;
         i_tick      :  IN STD_LOGIC;
         iv_RXdata   :  IN byte;
         ov_throttle :  OUT s2q16;
         ov_yaw      :  OUT s2q16;
         ov_pitch    :  OUT s2q16;
         ov_roll     :  OUT s2q16;
         ov_yawkp    :  OUT s6q16; 
         ov_yawki    :  OUT s6q16; 
         ov_yawkd    :  OUT s6q16;
         ov_pitchkp  :  OUT s6q16; 
         ov_pitchki  :  OUT s6q16; 
         ov_pitchkd  :  OUT s6q16;
         ov_rollkp   :  OUT s6q16; 
         ov_rollki   :  OUT s6q16; 
         ov_rollkd   :  OUT s6q16;
         o_enable    :  OUT std_logic
      );
end;

architecture bhv of BluetoothControl is

   type StateType is (init, dtype, parse);   --State machine states typedef
   signal state : StateType := init;
   signal throttle   :  s2q16;
   signal yaw        :  s2q16;
   signal pitch      :  s2q16;
   signal roll       :  s2q16;
   signal yawkp      :  s6q16;
   signal yawki      :  s6q16;
   signal yawkd      :  s6q16;
   signal pitchkp    :  s6q16;
   signal pitchki    :  s6q16;
   signal pitchkd    :  s6q16;
   signal rollkp     :  s6q16;
   signal rollki     :  s6q16;
   signal rollkd     :  s6q16;
   
   signal mode       :  std_logic_vector(3 downto 0);
   signal container  :  s6q16;
   
   signal enable     : std_logic;
   
begin
   ov_throttle <= throttle;
   ov_yaw      <= yaw;
   ov_pitch    <= pitch;
   ov_roll     <= roll;
   ov_yawkp    <= yawkp;
   ov_yawki    <= yawki;
   ov_yawkd    <= yawkd;
   ov_pitchkp  <= pitchkp;
   ov_pitchki  <= pitchki;
   ov_pitchkd  <= pitchkd;
   ov_rollkp   <= rollkp;
   ov_rollki   <= rollki;
   ov_rollkd   <= rollkd;
   o_enable    <= enable;

   state_machine:process(i_clk, i_rst)
   begin
      if(i_rst = '1') then
         throttle    <= (others => '0');
         yaw         <= (others => '0');
         pitch       <= (others => '0');
         roll        <= (others => '0');
         yawkp       <= (others => '0');
         yawki       <= (others => '0');
         yawkd       <= (others => '0');
         pitchkp     <= (others => '0');
         pitchki     <= (others => '0');
         pitchkd     <= (others => '0');
         rollkp      <= (others => '0');
         rollki      <= (others => '0');
         rollkd      <= (others => '0');
         mode        <= (others => '0');
         container   <= (others => '0');
         enable      <= '0';
         state <= init;
      elsif(rising_edge(i_clk)) then
         if(i_tick = '1') then
            case state is
               when init =>
                  container <= (others => '0');
                  state <= dtype;
                  case iv_RXdata is
                     when "01010100" => --T
                        mode <= "0000";
                        
                     when "01011001" => --Y
                        mode <= "0100";
                        
                     when "01010000" => --P
                        mode <= "1000";
                        
                     when "01010010" => --R
                        mode <= "1100";
                        
                     when "01000111" => --G
                        enable   <= '1';
                        mode     <= (others => '0');
                        state    <= init;
                        
                     when "01010011" => --S
                        enable   <= '0';
                        mode     <= (others => '0');
                        state    <= init;
                        
                     when others =>
                        mode     <= (others => '0');
                        state    <= init;
                        
                  end case;
                  
               when dtype =>
                  state <= parse;
                  case iv_RXdata is
                     when "01110110" => --v
                        mode(1 downto 0) <= "00";
                        
                     when "01110000" => --p
                        mode(1 downto 0) <= "01";
                        
                     when "01101001" => --i
                        mode(1 downto 0) <= "10";
                        
                     when "01100100" => --d
                        mode(1 downto 0) <= "11";
                        
                     when others =>
                        mode     <= (others => '0');
                        enable   <= '0';
                        state    <= init;
                        
                  end case;
                  
               when parse =>
                  if( unsigned(iv_RXdata) > 47 AND unsigned(iv_RXdata) < 58 ) then --digit
                     container <= resize(container * 10, container'length) + resize(signed('0'&iv_RXdata(3 downto 0)), container'length);
                     state <= parse;
                  elsif(iv_RXdata = "00001010") then --0A LF
                     case mode is
                        when "0000" => --Tv
                           throttle <= resize(container, throttle'length);
                           
                        when "0100" => --Yv
                           yaw <= resize(container, yaw'length);
                           
                        when "0101" => --Yp
                           yawkp <= resize(container, yawkp'length);
                           
                        when "0110" => --Yi
                           yawki <= resize(container, yawki'length);
                           
                        when "0111" => --Yd
                           yawkd <= resize(container, yawkd'length);
                           
                        when "1000" => --Pv
                           pitch <= resize(container, pitch'length);
                           
                        when "1001" => --Pp
                           pitchkp <= resize(container, pitchkp'length);
                           
                        when "1010" => --Pi
                           pitchki <= resize(container, pitchki'length);
                           
                        when "1011" => --Pd
                           pitchkd <= resize(container, pitchkd'length);
                           
                        when "1100" => --Rv
                           roll <= resize(container, roll'length);
                           
                        when "1101" => --Rp
                           rollkp <= resize(container, rollkp'length);
                           
                        when "1110" => --Ri
                           rollki <= resize(container, rollki'length);
                           
                        when "1111" => --Rd
                           rollkd <= resize(container, rollkd'length);
                           
                        when others =>
                           enable <= '0'; --error
                     end case;
                     state <= init;
                  else
                     state    <= init;
                     enable   <= '0';
                  end if;
                  
               when others =>
                  throttle    <= (others => '0');
                  yaw         <= (others => '0');
                  pitch       <= (others => '0');
                  roll        <= (others => '0');
                  yawkp       <= (others => '0');
                  yawki       <= (others => '0');
                  yawkd       <= (others => '0');
                  pitchkp     <= (others => '0');
                  pitchki     <= (others => '0');
                  pitchkd     <= (others => '0');
                  rollkp      <= (others => '0');
                  rollki      <= (others => '0');
                  rollkd      <= (others => '0');
                  mode        <= (others => '0');
                  container   <= (others => '0');
                  enable      <= '0';
                  state       <= init;
            end case;
         end if;
      end if;
   end process state_machine;
   
   
end bhv;