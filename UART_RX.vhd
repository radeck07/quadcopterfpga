--Name:     UART_RX
--Author:   Radoslaw Kalinka
--Date:     2017.03.08
--Desc:     Uart receiver module
--          Module works with 50MHz input clock
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_misc.all;
use ieee.numeric_std.ALL;
library work;
use work.regs.all;  

entity UART_RX is
   GENERIC(
      G_BIT_LENGTH     :integer := 434                      --Bit length in clk tacts
   );
   PORT(
      i_clk       : IN    std_logic;                        --input clk
      i_rst       : IN    std_logic;                        --reset signal
      i_RX        : IN    std_logic;                        --data input
      o_DRDY      : OUT   std_logic;                        --data ready
      ov_RXDATA   : OUT   std_logic_vector(7 downto 0)
   );
end;

architecture bhv of UART_RX is
   signal RX0, RX1, RX  : std_logic;
   signal RXTimer       : integer range 0 to (G_BIT_LENGTH + 1);--unsigned(15 downto 0);
   type StateType is (S0, S1, S2, S3);
   signal state         : StateType;
   signal DRDY          : std_logic;
   signal RXDATA        : std_logic_vector(7 downto 0);
   signal DATA          : std_logic_vector(7 downto 0);
   signal ByteTimer     : integer range 0 to (G_BIT_LENGTH + 1); --unsigned(15 downto 0);
   signal Probe         : unsigned(2 downto 0);
   signal ByteNr        : unsigned(2 downto 0);
   
begin
   process(i_clk, i_rst)
   begin
      if(i_rst = '1') then
         RXTimer  <= 0;--(others => '0');
         RX0      <= '0';
         RX1      <= '0';
         RX       <= '0';
      elsif(rising_edge(i_clk)) then
         RX0      <= i_RX;
         RX1      <= RX0;
         
         if(RX1 = RX) then
            RXTimer  <= 0;--(others => '0');
         elsif(RXTimer < G_BIT_LENGTH/16) then
            RXTimer  <= RXTimer + 1;
         else
            RXTimer  <= 0;--(others => '0');
            RX       <= not RX;
         end if;
      end if;
   end process;
   
   process(i_clk, i_rst)
   begin
      if(i_rst = '1') then
         state       <= S0;
         DRDY        <= '0';
         RXDATA      <= (others => '0');
         DATA        <= (others => '0');
         ByteTimer   <= 0;--(others => '0');
         Probe       <= (others => '0');
         ByteNr      <= (others => '0');
      else
         if(rising_edge(i_clk)) then
            case state is
               when S0 =>
                  DRDY        <= '0';
                  DATA        <= (others => '0');
                  ByteTimer   <= 0;--(others => '0');
                  Probe       <= (others => '0');
                  ByteNr      <= (others => '0');
                  if(RX = '0') then
                     state <= S1;
                  end if;
                  
               when S1 =>
                  if( ByteTimer = 7*G_BIT_LENGTH/16 OR ByteTimer = 8*G_BIT_LENGTH/16 OR ByteTimer = 9*G_BIT_LENGTH/16 ) then
                     ByteTimer   <= ByteTimer + 1;
                     if(RX = '1') then
                        Probe <= Probe + 1;
                     end if;
                  elsif( ByteTimer = G_BIT_LENGTH ) then
                     ByteTimer   <= 0;--(others => '0');
                     Probe       <= (others => '0');
                     if( Probe > 1 ) then
                        state <= S0;
                     else
                        state <= S2;
                     end if;
                  else
                     ByteTimer   <= ByteTimer + 1;
                  end if;
                  
               when S2 =>
                  if( ByteTimer = 7*G_BIT_LENGTH/16 OR ByteTimer = 8*G_BIT_LENGTH/16 OR ByteTimer = 9*G_BIT_LENGTH/16 ) then
                     ByteTimer   <= ByteTimer + 1;
                     if(RX = '1') then
                        Probe <= Probe + 1;
                     end if;
                  elsif( ByteTimer = G_BIT_LENGTH ) then
                     ByteTimer   <= 0;--(others => '0');
                     Probe       <= (others => '0');
                     ByteNr      <= ByteNr + 1;
                     if( Probe > 1 ) then
                        DATA  <= '1' & DATA(7 downto 1);
                     else
                        DATA  <= '0' & DATA(7 downto 1);
                     end if;
                     if( ByteNr = 7 ) then
                        state <= S3;
                     end if;
                  else
                     ByteTimer   <= ByteTimer + 1;
                  end if;
                  
               when S3 =>
                  state    <= S0;
                  DRDY     <= '1';
                  RXDATA   <= DATA;
                  
               when others =>
                  state       <= S0;
                  DRDY        <= '0';
                  RXDATA      <= (others => '0');
                  DATA        <= (others => '0');
                  ByteTimer   <= 0;--(others => '0');
                  Probe       <= (others => '0');
                  ByteNr      <= (others => '0');
                  
               end case;
         end if;
      end if;
   end process;
   
   o_DRDY      <= DRDY;
   ov_RXDATA   <= RXDATA;
   
end bhv;