library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.ALL;

entity RCDataController is
	PORT(
			clk		:	IN STD_LOGIC;
			tick		:	IN	STD_LOGIC;
			RXdata	:	IN	STD_LOGIC_vector(7 downto 0);
			emergencyThrottleIn	:	IN std_logic_vector(9 downto 0);
			throttle	:	OUT signed(31 downto 0):=(OTHERS => '0');
			yaw		:	OUT signed(31 downto 0):=(OTHERS => '0');
			pitch		:	OUT signed(31 downto 0):=(OTHERS => '0');
			roll		:	OUT signed(31 downto 0):=(OTHERS => '0');	
			DRDY		:	OUT std_logic:='1'											--Goes low at new valid data recognition
		);
end;

architecture bhv of RCDataController is

	type StateType is (th, tl, yh, yl, ph, pl, rh, rl, chksum);	--State machine states typedef
	signal state : StateType; 												--State machine declaration
	signal T, Y, P, R : std_logic_vector(9 downto 0);				--Temporary value signals (for checksum)
	signal throttle_raw, yaw_raw, pitch_raw, roll_raw : std_logic_vector(9 downto 0);
	signal timer			:	unsigned(31 DOWNTO 0) := (others => '0');	--watchdog timer
	signal emergencyOn	: std_logic := '0';
	
	
begin
	throttle(31 downto 16)	<=	"0000000000000000";
	throttle(15 downto 6)	<=	signed(throttle_raw);
	throttle(5 downto 0)		<= "000000" when throttle_raw(0) = '0' else "111111";
	
	yaw(31 downto 16)			<=	"0000000000000000" when yaw_raw(9) = '1' else "1111111111111111";
	yaw(15 downto 7)			<=	signed(yaw_raw(8 downto 0));
	yaw(6 downto 0)			<=	"0000000" when yaw_raw(0) = '0' else "1111111";
	
	pitch(31 downto 16)		<=	"0000000000000000" when pitch_raw(9) = '1' else "1111111111111111";
	pitch(15 downto 7)		<=	signed(pitch_raw(8 downto 0));
	pitch(6 downto 0)			<=	"0000000" when pitch_raw(0) = '0' else "1111111";
	
	roll(31 downto 16)		<=	"0000000000000000" when roll_raw(9) = '1' else "1111111111111111";
	roll(15 downto 7)			<=	signed(roll_raw(8 downto 0));
	roll(6 downto 0)			<=	"0000000" when roll_raw(0) = '0' else "1111111";

	state_machine: process(RXdata, state, clk)
	begin
		if falling_edge(clk) then
			if(timer < to_integer(to_unsigned(2500000, 32))) then	--timeout = 100ms
				timer <= timer + 1;
			else
				emergencyOn <= '1';
				timer <= (others => '0');
			end if;
			
			DRDY <= '1';
			
			if (tick = '0') then
				case state is
					--Throttle higher bits--
					when th =>
						DRDY <= '1';
						if(RXdata(7 downto 5) = "000") then
							state <= tl;
							T(9 downto 5) <= RXdata(4 downto 0);
						else
							state <= th;
						end if;
					------------------------
					
					--Throttle lower bits--
					when tl =>
						if(RXdata(7 downto 5) = "001") then
							state <= yh;
							T(4 downto 0) <= RXdata(4 downto 0);
						else
							state <= th;
						end if;
					-----------------------
					
					--Yaw higher bits--
					when yh =>
						if(RXdata(7 downto 5) = "010") then
							state <= yl;
							Y(9 downto 5) <= RXdata(4 downto 0);
						else
							state <= th;
						end if;
					-------------------
					
					--Yaw lower bits--
					when yl =>
						if(RXdata(7 downto 5) = "011") then
							state <= ph;
							Y(4 downto 0) <= RXdata(4 downto 0);
						else
							state <= th;
						end if;
					------------------
					
					--Pitch higher bits--
					when ph =>
						if(RXdata(7 downto 5) = "100") then
							state <= pl;
							P(9 downto 5) <= RXdata(4 downto 0);
						else
							state <= th;
						end if;
					---------------------
					
					--Pitch lower bits--
					when pl =>
						if(RXdata(7 downto 5) = "101") then
							state <= rh;
							P(4 downto 0) <= RXdata(4 downto 0);
						else
							state <= th;
						end if;
					--------------------
					
					--Roll higher bits--
					when rh =>
						if(RXdata(7 downto 5) = "110") then
							state <= rl;
							R(9 downto 5) <= RXdata(4 downto 0);
						else
							state <= th;
						end if;
					--------------------
					
					--Roll lower bits--
					when rl =>
						if(RXdata(7 downto 5) = "111") then
							state <= chksum;
							R(4 downto 0) <= RXdata(4 downto 0);
						else
							state <= th;
						end if;
					-------------------
					
					--CHECKSUM--
					when chksum =>
						state <= th;
						throttle_raw <= T;
						yaw_raw <= Y;
						pitch_raw <= P;
						roll_raw <= R;
						DRDY <= '0';
						timer <= (others => '0');
						emergencyOn <= '0';
						
				end case;
			end if;
			
			if(emergencyOn = '1') then
				throttle_raw	<= emergencyThrottleIn;
				yaw_raw			<= "1000000000";
				pitch_raw		<= "1000000000";
				roll_raw			<= "1000000000";
			end if;
		end if;
	end process state_machine;
end bhv;