--Name:     AHRSDataRcv
--Author:   Radoslaw Kalinka
--Date:     2017.03.05
--Desc:     AHRS Data Receiver module testbench
--          Module works with 50MHz input clock
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.ALL;

entity AHRSDataRcv_tb is
end;

architecture bhv of AHRSDataRcv_tb is

   component AHRSDataRcv is
      PORT(
         i_clk      :  IN STD_LOGIC;
         i_rst      :  IN STD_LOGIC;
         i_tick     :  IN STD_LOGIC;
         iv_RXdata   :  IN std_logic_vector(7 downto 0);
         ov_yaw      :  OUT std_logic_vector(31 downto 0);
         ov_pitch    :  OUT std_logic_vector(31 downto 0);
         ov_roll     :  OUT std_logic_vector(31 downto 0); 
         o_DRDY     :  OUT std_logic                               --Goes low at new valid data recognition
      );
   end component AHRSDataRcv;
   
   signal clk, rst, tick, DRDY : std_logic;
   signal RXdata : std_logic_vector(7 downto 0);
   signal yaw, pitch, roll : std_logic_vector(31 downto 0);
   
begin

   AHRSDataRcv_inst: AHRSDataRcv port map (  i_clk    => clk,
                                             i_rst    => rst,
                                             i_tick      => tick,
                                             iv_RXdata    => RXdata,
                                             ov_yaw       => yaw,
                                             ov_pitch     => pitch,
                                             ov_roll      => roll,
                                             o_DRDY      => DRDY
                                          );
   
   clk_gen: process                
   begin                           
      clk <= '0';                 
      wait for 10 ns;             
      clk <= '1';                 
      wait for 10 ns;
   end process;
   
   stim_process: process
   begin
      rst      <= '1';
      tick  <= '0';
      RXdata   <= (others => '0');
      wait for 20 ns;
      rst      <= '0';
      wait for 20 ns;
      tick  <= '1';
      RXdata <= "00100011";
      wait for 20 ns;
      tick  <= '0';
      RXdata   <= (others => '0');
      wait for 20 ns;
      RXdata <= "01010010";
      tick  <= '1';
      wait for 20 ns;
      tick  <= '0';
      RXdata   <= (others => '0');
      wait for 20 ns;
      RXdata <= "01010000";
      tick  <= '1';
      wait for 20 ns;
      tick  <= '0';
      RXdata   <= (others => '0');
      wait for 20 ns;
      RXdata <= "01011001";
      tick  <= '1';
      wait for 20 ns;
      tick  <= '0';
      RXdata   <= (others => '0');
      wait for 20 ns;
      RXdata <= "00101011";
      tick  <= '1';
      wait for 20 ns;
      tick  <= '0';
      RXdata   <= (others => '0');
      wait for 20 ns;
      RXdata <= std_logic_vector(to_unsigned(49, 8));
      tick  <= '1';
      wait for 20 ns;
      tick  <= '0';
      RXdata   <= (others => '0');
      wait for 20 ns;
      RXdata <= std_logic_vector(to_unsigned(56, 8));
      tick  <= '1';
      wait for 20 ns;
      tick  <= '0';
      RXdata   <= (others => '0');
      wait for 20 ns;
      RXdata <= std_logic_vector(to_unsigned(48, 8));
      tick  <= '1';
      wait for 20 ns;
      tick  <= '0';
      RXdata   <= (others => '0');
      wait for 20 ns;
      RXdata <= "00101110";
      tick  <= '1';
      wait for 20 ns;
      tick  <= '0';
      RXdata   <= (others => '0');
      wait for 20 ns;
      RXdata <= std_logic_vector(to_unsigned(48, 8));
      tick  <= '1';
      wait for 20 ns;
      tick  <= '0';
      RXdata   <= (others => '0');
      wait for 20 ns;
      RXdata <= std_logic_vector(to_unsigned(48, 8));
      tick  <= '1';
      wait for 20 ns;
      tick  <= '0';
      RXdata   <= (others => '0');
      wait for 20 ns;
      RXdata <= "00101100";
      tick  <= '1';
      wait for 20 ns;
      tick  <= '0';
      RXdata   <= (others => '0');
      wait for 20 ns;
      RXdata <= "00101101";
      tick  <= '1';
      wait for 20 ns;
      tick  <= '0';
      RXdata   <= (others => '0');
      wait for 20 ns;
      RXdata <= std_logic_vector(to_unsigned(49, 8));
      tick  <= '1';
      wait for 20 ns;
      tick  <= '0';
      RXdata   <= (others => '0');
      wait for 20 ns;
      RXdata <= std_logic_vector(to_unsigned(56, 8));
      tick  <= '1';
      wait for 20 ns;
      tick  <= '0';
      RXdata   <= (others => '0');
      wait for 20 ns;
      RXdata <= std_logic_vector(to_unsigned(48, 8));
      tick  <= '1';
      wait for 20 ns;
      tick  <= '0';
      RXdata   <= (others => '0');
      wait for 20 ns;
      RXdata <= "00101110";
      tick  <= '1';
      wait for 20 ns;
      tick  <= '0';
      RXdata   <= (others => '0');
      wait for 20 ns;
      RXdata <= std_logic_vector(to_unsigned(48, 8));
      tick  <= '1';
      wait for 20 ns;
      tick  <= '0';
      RXdata   <= (others => '0');
      wait for 20 ns;
      RXdata <= std_logic_vector(to_unsigned(48, 8));
      tick  <= '1';
      wait for 20 ns;
      tick  <= '0';
      RXdata   <= (others => '0');
      wait for 20 ns;
      RXdata <= "00101100";
      tick  <= '1';
      wait for 20 ns;
      tick  <= '0';
      RXdata   <= (others => '0');
      wait for 20 ns;
      RXdata <= "00101011";
      tick  <= '1';
      wait for 20 ns;
      tick  <= '0';
      RXdata   <= (others => '0');
      wait for 20 ns;
      RXdata <= std_logic_vector(to_unsigned(49, 8));
      tick  <= '1';
      wait for 20 ns;
      tick  <= '0';
      RXdata   <= (others => '0');
      wait for 20 ns;
      RXdata <= std_logic_vector(to_unsigned(56, 8));
      tick  <= '1';
      wait for 20 ns;
      tick  <= '0';
      RXdata   <= (others => '0');
      wait for 20 ns;
      RXdata <= std_logic_vector(to_unsigned(48, 8));
      tick  <= '1';
      wait for 20 ns;
      tick  <= '0';
      RXdata   <= (others => '0');
      wait for 20 ns;
      RXdata <= "00101110";
      tick  <= '1';
      wait for 20 ns;
      tick  <= '0';
      RXdata   <= (others => '0');
      wait for 20 ns;
      RXdata <= std_logic_vector(to_unsigned(48, 8));
      tick  <= '1';
      wait for 20 ns;
      tick  <= '0';
      RXdata   <= (others => '0');
      wait for 20 ns;
      RXdata <= std_logic_vector(to_unsigned(48, 8));
      tick  <= '1';
      wait for 20 ns;
      tick  <= '0';
      RXdata   <= (others => '0');
      wait for 20 ns;
      RXdata <= "00101100";
      tick  <= '1';
      wait for 20 ns;
      tick  <= '0';
      RXdata   <= (others => '0');
      wait for 20 ns;
      RXdata <= "00001101";
      tick  <= '1';
      wait for 20 ns;
      tick  <= '0';
      RXdata   <= (others => '0');
      wait for 20 ns;
      RXdata <= "00001010";
      tick  <= '1';
      wait for 20 ns;
      tick  <= '0';
      RXdata   <= (others => '0');
      wait;
   end process;
   
end bhv;