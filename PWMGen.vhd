library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.ALL; 

entity PWMGen is
	PORT(
			clk				:	IN		STD_LOGIC;
			dutyCycle		:	IN		STD_LOGIC_VECTOR(15 DOWNTO 0);
			periodVal		:	IN		STD_LOGIC_VECTOR(15 DOWNTO 0);
			reset				:	IN		STD_LOGIC;
			PWMOut			:	OUT	STD_LOGIC
		);
end;

architecture bhv of PWMGen is
	signal timer			:	unsigned(15 DOWNTO 0) := (others => '0');
	signal dutyCycleReg	:	STD_LOGIC_VECTOR(15 DOWNTO 0) := (others => '0');
	begin
		PWMOut <= '1' when timer < to_integer(unsigned(dutyCycleReg)) else
					 '0';
		process(clk, periodVal, reset) begin --czy nie za duzo w liscie czulosci?
			if(falling_edge(clk)) then
				if(reset = '0') then
					timer <= (others => '0');
				else
					if(timer < to_integer(unsigned(periodVal))) then
					timer <= timer + 1;
					else
						timer <= (others => '0');
					end if;
				end if;
				if(timer = to_unsigned(0, 16)) then
					dutyCycleReg <= dutyCycle;
				end if;
			end if;
		end process;
end bhv;
	