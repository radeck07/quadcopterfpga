--Name:     Divider
--Author:   Radoslaw Kalinka
--Date:     2017.10.28
--Desc:     Divider Module 
--          Module works with 50MHz input clock
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_misc.all;
use ieee.numeric_std.ALL;

library work; 

entity Divider is
   PORT(
      i_clk       : IN STD_LOGIC;
      i_rst       : IN STD_LOGIC;
      i_tick      : IN std_logic;        --start division
      iv_Numer    : IN std_logic_vector; --numerator
      iv_Denomin  : IN std_logic_vector; --denominator
      ov_Result   : OUT std_logic_vector;--result
      o_DRDY      : OUT std_logic;
      o_BSY       : OUT std_logic
   );
end entity;

architecture bhv of Divider is
   type state_type is (ready, calc, stop);
   signal state   : state_type := ready;
   signal numerator, denominator : signed(iv_Numer'length + iv_Denomin'length - 1 downto 0); 
   signal result : signed(iv_Numer'range);
   signal result_out : signed(ov_Result'range);
   signal DRDY, BSY : std_logic;
   signal shift_cnt : integer range 0 to iv_Numer'length + iv_Denomin'length - 1;
   signal sign : std_logic;
   
begin

   ov_Result   <= std_logic_vector(result_out);
   o_BSY       <= BSY;
   o_DRDY      <= DRDY;
   
   process(i_rst, i_clk)
   begin
      if(i_rst = '1') then
         DRDY  <= '0';
         BSY   <= '0';
         result_out <= (others => '0');
         result <= (others => '0');
         numerator <= (others => '0');
         denominator <= (others => '0');
         shift_cnt <= 0;
         sign <= '0';
         state   <= ready;
      elsif(rising_edge(i_clk)) then
         case state is
            when ready =>
               DRDY  <= '0';
               numerator <= abs(resize(signed(iv_Numer), numerator'length));
               denominator <= abs(shift_left(resize(signed(iv_Denomin), denominator'length), iv_Numer'length - 1));
			   result <= (others => '0');
               if(signed(iv_Numer) < 0) xor (signed(iv_Denomin) < 0) then
                  sign <= '1';
               else
                  sign <= '0';
               end if;
               shift_cnt <= iv_Numer'length - 1;
               if(i_tick = '1') then
                  BSY   <= '1';
                  state <= calc;
               end if;
               
            when calc =>
               if(numerator >= denominator) then
                  result(shift_cnt) <= '1';
                  numerator <= numerator - denominator;
               else
                  result(shift_cnt) <= '0';
               end if;
               if(shift_cnt > 0) then
                  shift_cnt <= shift_cnt - 1;
                  denominator <= shift_right(denominator, 1);
               else
                  state <= stop;
               end if;
               
            when stop =>
               if(i_tick = '0') then
                  if(sign = '1') then
                     result_out <= resize(to_signed(0, result_out'length) - result, result_out'length);
                  else
                     result_out <= resize(result, result_out'length);
                  end if;
                  state <= ready;
                  DRDY  <= '1';
                  BSY   <= '0';
               end if;
               
               
            when others =>
               DRDY  <= '0';
               BSY   <= '0';
               result_out <= (others => '0');
               result <= (others => '0');
               numerator <= (others => '0');
               denominator <= (others => '0');
               shift_cnt <= 0;
               sign <= '0';
               state   <= ready;
               
         end case;
      end if;
   end process;
end;