--Name:     ResetSynchronizer
--Author:   Radoslaw Kalinka
--Date:     2016.06.04
--Desc:     Reset Synchronizer Module 
--          Module works with 50MHz input clock
--          https://www.altera.com/content/dam/altera-www/global/en_US/pdfs/literature/an/an545.pdf

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_misc.all;
use ieee.numeric_std.ALL;
library work;
use work.regs.all; 

entity ResetSynchronizer is
   PORT(
      i_clk : IN std_logic;
      i_rst : IN std_logic;
      o_rst : OUT std_logic
   );
end;

architecture bhv of ResetSynchronizer is
signal D1 : std_logic := '1';
signal D2 : std_logic := '1';

begin
   process(i_clk, i_rst)
   begin
      if(i_rst = '1') then
         D1 <= '1';
         D2 <= '1';
      elsif(rising_edge(i_clk)) then
         D1 <= '0';
         D2 <= D1;
      end if;
   end process;
   
   o_rst <= D2;
end;