--Name:     SPI_tb
--Author:   Radoslaw Kalinka
--Date:     2017.06.07
--Desc:     Uart receiver module testbench
--          Module works with 50MHz input clock
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.ALL; 

entity SPI_tb is
end;

architecture bhv of SPI_tb is
component SPI is
   PORT(
      i_clk       : IN STD_LOGIC;
      i_rst       : IN STD_LOGIC;
      i_tick      : IN std_logic;                     --spi send request
      iv_DataOut  : IN std_logic_vector(15 downto 0); --data to send
      iv_BitLen   : IN std_logic_vector(7 downto 0);
      iv_DataLen  : IN std_logic_vector(7 downto 0);
      i_MISO      : IN std_logic;
      o_MOSI      : OUT std_logic;
      o_SCK       : OUT std_logic;
      o_NCS       : OUT std_logic;
      o_DRDY      : OUT std_logic;
      o_BSY       : OUT std_logic;
      ov_DataIn   : OUT std_logic_vector(95 downto 0)
   );
end component SPI;
   
   signal clk       : std_logic;                  
   signal rst       : std_logic;                  
   signal MISO    : std_logic;
   signal MOSI    : std_logic;
   signal SCK     : std_logic;
   signal NCS     : std_logic;
   signal DRDY    : std_logic;
   signal BSY     : std_logic;
   signal DataIn  : std_logic_vector(95 downto 0);
   signal tick    : std_logic;
   
   
begin
                           
   SPI_inst: SPI port map (
                           i_clk       => clk,
                           i_rst       => rst,
                           i_tick      => tick,
                           iv_DataOut  => "1011010011000011",
                           iv_BitLen   => "00000010",
                           iv_DataLen  => "00010000",
                           i_MISO      => MISO,
                           o_MOSI      => MOSI,
                           o_SCK       => SCK,
                           o_NCS       => NCS,
                           o_DRDY      => DRDY,
                           o_BSY       => BSY,
                           ov_DataIn   => DataIn
                         );  
   
   MISO  <= MOSI;
   
   clk_gen: process                
   begin                           
      clk <= '0';                 
      wait for 10 ns;             
      clk <= '1';                 
      wait for 10 ns;
   end process;
   
   stim_process: process
   begin
      rst <= '1';
      tick <= '0';
      wait for 20 ns;
      rst <= '0';
      wait for 20 ns;
      tick  <= '1';
      
      wait;
   end process;
   
end bhv;

