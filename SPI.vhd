--Name:     SPI
--Author:   Radoslaw Kalinka
--Date:     2017.06.06
--Desc:     SPI Module 
--          Module works with 50MHz input clock
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_misc.all;
use ieee.numeric_std.ALL;
library work;
use work.regs.all;  

entity SPI is
   PORT(
      i_clk       : IN STD_LOGIC;
      i_rst       : IN STD_LOGIC;
      i_tick      : IN std_logic;                     --spi send request
      iv_DataOut  : IN std_logic_vector(15 downto 0); --data to send
      iv_BitLen   : IN std_logic_vector(7 downto 0);
      iv_DataLen  : IN std_logic_vector(7 downto 0);
      i_MISO      : IN std_logic;
      o_MOSI      : OUT std_logic;
      o_SCK       : OUT std_logic;
      o_NCS       : OUT std_logic;
      o_DRDY      : OUT std_logic;
      o_BSY       : OUT std_logic;
      ov_DataIn   : OUT std_logic_vector(111 downto 0)
   );
end entity;

architecture bhv of SPI is
   type state_type is (ready, transmit, stop);
   signal state   : state_type := ready;
   signal clkcnt  : unsigned(7 downto 0);  --spi clock generator counter
   signal bitcnt  : unsigned(7 downto 0);
   signal MOSI    : std_logic;
   signal SCK     : std_logic;
   signal NCS     : std_logic;
   signal DRDY    : std_logic;
   signal DataIn  : std_logic_vector(111 downto 0);
   signal DataOut : std_logic_vector(15 downto 0);
   signal BSY     : std_logic;
   
begin

   o_MOSI   <= MOSI;
   o_SCK    <= SCK; 
   o_NCS    <= NCS; 
   o_DRDY   <= DRDY;
   o_BSY    <= BSY;
   ov_DataIn <= DataIn;
   
   process(i_rst, i_clk)
   begin
   if(i_rst = '1') then
      MOSI  <= '1';
      SCK   <= '1';
      NCS   <= '1';
      DRDY  <= '0';
      BSY   <= '0';
      bitcnt   <= (others => '0');
      clkcnt   <= (others => '0');
      DataIn   <= (others => '0');
      DataOut  <= (others => '0');
      state   <= ready;
   elsif(rising_edge(i_clk)) then
      case state is
         when ready =>
            MOSI  <= '1';
            SCK   <= '1';
            NCS   <= '1';
            DRDY  <= '0';
            BSY   <= '0';
            
            DataOut  <= iv_DataOut;
            clkcnt   <= (others => '0');
            bitcnt   <= (others => '0');
            DataIn   <= (others => '0');
            
            if(i_tick = '1') then
               NCS   <= '0';
               BSY   <= '1';
               state <= transmit;
            end if;
            
         when transmit =>
            if(clkcnt = 0) then
               MOSI <= DataOut(15);
               DataOut  <= DataOut(14 downto 0) & '0';
               SCK   <= '0';
            elsif(clkcnt = unsigned(iv_BitLen)/2) then
               DataIn  <= DataIn(DataIn'length-2 downto 0) & i_MISO;
               SCK   <= '1';
            end if;
            
            
            if(clkcnt < unsigned(iv_BitLen) - 1) then
               clkcnt <= clkcnt + 1;
            else
               if(bitcnt < unsigned(iv_DataLen) - 1) then
                  bitcnt <= bitcnt + 1;
               else
                  state <= stop;
               end if;
               clkcnt <= (others => '0');
            end if;
            
            
         when stop =>
            if(clkcnt < 25) then
               clkcnt <= clkcnt + 1;
            else
            NCS <= '1';
               if(i_tick = '0') then
                  state <= ready;
                  DRDY  <= '1';
                  BSY   <= '0';
               end if;
            end if;
            
         when others =>
            MOSI  <= '1';
            SCK   <= '1';
            NCS   <= '1';
            DRDY  <= '0';
            BSY   <= '0';
            bitcnt   <= (others => '0');
            clkcnt   <= (others => '0');
            DataIn   <= (others => '0');
            DataOut  <= (others => '0');
            state   <= ready;
      end case;
   end if;
   end process;
end;