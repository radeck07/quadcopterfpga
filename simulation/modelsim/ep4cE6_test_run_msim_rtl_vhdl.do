transcript on
if {[file exists rtl_work]} {
	vdel -lib rtl_work -all
}
vlib rtl_work
vmap work rtl_work

vlog -vlog01compat -work work +incdir+C:/Users/User/Desktop/flight_controller2/quadcopterfpga/db {C:/Users/User/Desktop/flight_controller2/quadcopterfpga/db/pll_altpll.v}
vcom -2008 -work work {C:/Users/User/Desktop/flight_controller2/quadcopterfpga/regs.vhd}
vcom -2008 -work work {C:/Users/User/Desktop/flight_controller2/quadcopterfpga/PLL.vhd}
vcom -2008 -work work {C:/Users/User/Desktop/flight_controller2/quadcopterfpga/UART_RX.vhd}
vcom -2008 -work work {C:/Users/User/Desktop/flight_controller2/quadcopterfpga/PID.vhd}
vcom -2008 -work work {C:/Users/User/Desktop/flight_controller2/quadcopterfpga/MotorController.vhd}
vcom -2008 -work work {C:/Users/User/Desktop/flight_controller2/quadcopterfpga/test.vhd}
vcom -2008 -work work {C:/Users/User/Desktop/flight_controller2/quadcopterfpga/BluetoothControl.vhd}
vcom -2008 -work work {C:/Users/User/Desktop/flight_controller2/quadcopterfpga/RemoteControlPPM.vhd}
vcom -2008 -work work {C:/Users/User/Desktop/flight_controller2/quadcopterfpga/RazorAHRSDataRcv.vhd}
vcom -2008 -work work {C:/Users/User/Desktop/flight_controller2/quadcopterfpga/ResetSynchronizer.vhd}
vcom -2008 -work work {C:/Users/User/Desktop/flight_controller2/quadcopterfpga/SPI.vhd}

