package require ::quartus::project

set_location_assignment PIN_25 -to sys_clk
set_location_assignment PIN_138 -to LED[0]
set_location_assignment PIN_141 -to LED[1]
set_location_assignment PIN_142 -to LED[2]
set_location_assignment PIN_143 -to LED[3]
set_location_assignment PIN_144 -to LED[4]
set_location_assignment PIN_1 -to LED[5]
set_location_assignment PIN_2 -to LED[6]
set_location_assignment PIN_3 -to LED[7]
