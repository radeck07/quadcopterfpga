--Name:     MotorController
--Author:   Radoslaw Kalinka
--Date:     2016.10.23
--Desc:     Motor controller module that converts input fixed point 32 (16,16) bit number to PPM: 1ms to 2ms pulses in 2.5ms cycle
--          Module works with 50MHz input clock

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_misc.all;
use ieee.numeric_std.ALL;
library work;
use work.regs.all; 

entity MotorController is
   PORT(
      i_clk          : IN std_logic;
      i_rst          : IN std_logic;
      i_enable       : IN std_logic;
      iv_speedFL     : IN s2q16; 
      iv_speedFR     : IN s2q16;
      iv_speedRL     : IN s2q16;
      iv_speedRR     : IN s2q16;
      o_timer_pulse  : OUT std_logic;
      o_PPMFL        : OUT std_logic;
      o_PPMFR        : OUT std_logic;
      o_PPMRL        : OUT std_logic;
      o_PPMRR        : OUT std_logic
   );
end;

architecture bhv of MotorController is
   signal timer                                    : integer range 0 to 125000;
   signal speedFLv, speedFRv, speedRLv, speedRRv   : integer range 0 to 100000;--unsigned(16 downto 0);
   begin
      process(i_clk, i_rst)
      begin
         if(i_rst = '1') then
            timer    <= 0;
            o_PPMFL  <= '0';
            o_PPMFR  <= '0';
            o_PPMRL  <= '0';
            o_PPMRR  <= '0';
         elsif(rising_edge(i_clk)) then
            
            if(timer < 125000) then
               timer <= timer + 1;
            else
               timer <= 0;
            end if;
            
            --generate tick for PIDs
            o_timer_pulse <= '0';
            if(i_enable = '1') then
               if(timer = 1) then
                  speedFLv <= to_integer(shift_right(iv_speedFL * 50000, 16)) + 50000;
               elsif(timer = 2) then
                  speedFRv <= to_integer(shift_right(iv_speedFR * 50000, 16)) + 50000;
               elsif(timer = 3) then
                  speedRLv <= to_integer(shift_right(iv_speedRL * 50000, 16)) + 50000;
               elsif(timer = 4) then
                  speedRRv <= to_integer(shift_right(iv_speedRR * 50000, 16)) + 50000;
               elsif(timer = 100000) then
                  o_timer_pulse <= '1';
               end if;
               
               if(timer < speedFLv) then
                  o_PPMFL <= '1';
               else
                  o_PPMFL <= '0';
               end if;
               if(timer < speedFRv) then
                  o_PPMFR <= '1';
               else
                  o_PPMFR <= '0';
               end if;
               if(timer < speedRLv) then
                  o_PPMRL <= '1';
               else
                  o_PPMRL <= '0';
               end if;
               if(timer < speedRRv) then
                  o_PPMRR <= '1';
               else
                  o_PPMRR <= '0';
               end if;
            else
               if(timer < 50000) then
                  o_PPMFL <= '1';
                  o_PPMFR <= '1';
                  o_PPMRL <= '1';
                  o_PPMRR <= '1';
               else
                  o_PPMFL <= '0';
                  o_PPMFR <= '0';
                  o_PPMRL <= '0';
                  o_PPMRR <= '0';
               end if;
            end if;
         end if;
      end process;
end;	