--Name:     Sqrt
--Author:   Radoslaw Kalinka
--Date:     2017.10.28
--Desc:     Sqrt Module 
--          Module works with 50MHz input clock
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_misc.all;
use ieee.numeric_std.ALL;

library work; 

entity Sqrt is
   PORT(
      i_clk       : IN STD_LOGIC;
      i_rst       : IN STD_LOGIC;
      i_tick      : IN std_logic;        --start division
      iv_input    : IN std_logic_vector; --numerator
      ov_sqrt     : OUT std_logic_vector;--result
      o_DRDY      : OUT std_logic;
      o_BSY       : OUT std_logic
   );
end entity;

architecture bhv of Sqrt is
   type state_type is (ready, calc, stop);
   signal state   : state_type := ready;
   signal input   : unsigned(iv_input'range);
   signal bit_no  : unsigned(iv_input'range);
   signal result  : unsigned(iv_input'range);
   signal result_out : unsigned(ov_sqrt'range);
   signal DRDY, BSY  : std_logic;
   
begin

   ov_sqrt  <= std_logic_vector(result_out);
   o_BSY    <= BSY;
   o_DRDY   <= DRDY;
   
   process(i_rst, i_clk)
   begin
      if(i_rst = '1') then
         DRDY        <= '0';
         BSY         <= '0';
         input       <= (others => '0');
         bit_no      <= (others => '0');
         result_out  <= (others => '0');
         result      <= (others => '0');
         state       <= ready;
      elsif(rising_edge(i_clk)) then
         case state is
            when ready =>
               DRDY  <= '0';
               input <= unsigned(iv_input);
               bit_no <= (iv_input'length-2 => '1', others => '0');
               result <= (others => '0');
               if(i_tick = '1') then
                  BSY   <= '1';
                  state <= calc;
               end if;
               
            when calc =>
               if(bit_no > 0) then
                  if (input >= (result + bit_no)) then
                     input <= input - (result + bit_no);
                     result <= shift_right(result, 1) + bit_no;
                  else
                     result <= shift_right(result, 1);
                  end if;
                  bit_no <= shift_right(bit_no, 2);
               else
                  state <= stop;
               end if;
               
            when stop =>
               if(i_tick = '0') then
                  result_out <= resize(result, result_out'length);
                  state <= ready;
                  DRDY  <= '1';
                  BSY   <= '0';
               end if;
               
               
            when others =>
               DRDY        <= '0';
               BSY         <= '0';
               input       <= (others => '0');
               bit_no      <= (others => '0');
               result_out  <= (others => '0');
               result      <= (others => '0');
               state       <= ready;
               
         end case;
      end if;
   end process;
end;