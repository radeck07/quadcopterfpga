--Name:     AHRSDataRcv
--Author:   Radoslaw Kalinka
--Date:     2016.10.17
--Desc:     AHRS Data Receiver module 
--          Module works with 50MHz input clock
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.ALL;

entity AHRSDataRcv is
   PORT(
         i_clk      :  IN STD_LOGIC;
         i_rst      :  IN STD_LOGIC;
         i_tick     :  IN STD_LOGIC;
         iv_RXdata  :  IN std_logic_vector(7 downto 0);
         ov_yaw     :  OUT std_logic_vector(31 downto 0);
         ov_pitch   :  OUT std_logic_vector(31 downto 0);
         ov_roll    :  OUT std_logic_vector(31 downto 0); 
         o_DRDY     :  OUT std_logic                               --Goes low at new valid data recognition
      );
end;

architecture bhv of AHRSDataRcv is

   type StateType is (hash, R, P, Y, R_sign, R_h, R_t, R_u, R_dot, R_ft, R_fh, R_comma, P_sign, P_h, P_t, P_u, P_dot, P_ft, P_fh, P_comma, Y_sign, Y_h, Y_t, Y_u, Y_dot, Y_ft, Y_fh, Y_comma, ZD, ZA);   --State machine states typedef
   signal state : StateType := hash;                                    --State machine declaration
   signal vRp, vPp, vYp       : std_logic;
   signal vR, vP, vY          : unsigned(31 downto 0);
   signal yaw, pitch, roll    : signed(31 downto 0) := (others=>'0');
   signal DRDY                : std_logic := '0';
begin
   ov_yaw      <= std_logic_vector(yaw);
   ov_pitch    <= std_logic_vector(pitch);
   ov_roll     <= std_logic_vector(roll);
   o_DRDY      <= DRDY;
   state_machine:process(i_clk, i_rst)
   begin
      if(i_rst = '1') then
         DRDY  <= '0';
         yaw   <= (others => '0');
         pitch <= (others => '0');
         roll  <= (others => '0');
         state <= hash;
         vR    <= (others => '0');
         vP    <= (others => '0');
         vY    <= (others => '0');
         vRp   <= '0';
         vPp   <= '0';
         vYp   <= '0';
      elsif(rising_edge(i_clk)) then
         DRDY <= '0';
         if(i_tick = '1') then
            case state is
            
            --FRAME FORMAT
            --#RPYSRRR.RR,SPPP.PP,SYYY.YY,\r\n
            
               when hash      =>                --Wait for # character.
                  if(iv_RXdata = "00100011") then
                     state <= R;
                  end if;
                  
               when R         =>                --Wait for R character. 
                  if(iv_RXdata = "01010010") then
                     state <= P;
                  else
                     state <= hash;
                  end if;
                  
               when P         =>                --Wait for P character. 
                  if(iv_RXdata = "01010000") then
                     state <= Y;
                  else
                     state <= hash;
                  end if;
                  
               when Y         =>                --Wait for Y character. 
                  if(iv_RXdata = "01011001") then
                     state <= R_sign;
                  else
                     state <= hash;
                  end if;
                  
               when R_sign    =>                --Wait for sign of Roll value character. 
                  state <= R_h;
                  if(iv_RXdata = "00101011") then
                     vRp <= '1';
                  elsif(iv_RXdata = "00101101") then
                     vRp <= '0';
                  else
                     state <= hash;
                  end if;
               when R_h       =>                --Wait for hundreds digit of Roll. 
                  state <= R_t;
                  if(unsigned(iv_RXdata) > 47 AND unsigned(iv_RXdata) < 50) then
                     vR <= resize(unsigned(iv_RXdata(3 downto 0)), vR'length);
                  else
                     state <= hash;
                  end if;
               when R_t       =>                --Wait for tens digit of Roll.
                  state <= R_u;
                  if(unsigned(iv_RXdata) > 47 AND unsigned(iv_RXdata) < 58) then
                     vR <= resize( vR * to_unsigned(10, vR'length), vR'length ) + resize(unsigned(iv_RXdata(3 downto 0)), vR'length);
                  else
                     state <= hash;
                  end if;
               when R_u       =>                --Wait for units digit of Roll.
                  state <= R_dot;
                  if(unsigned(iv_RXdata) > 47 AND unsigned(iv_RXdata) < 58) then
                     vR <= resize( vR * to_unsigned(10, vR'length), vR'length ) + resize(unsigned(iv_RXdata(3 downto 0)), vR'length);
                  else
                     state <= hash;
                  end if;
               when R_dot     =>
                  if(iv_RXdata = "00101110") then
                     state <= R_ft;
                  else
                     state <= hash;
                  end if;
               when R_ft      =>
                  state <= R_fh;
                  if(unsigned(iv_RXdata) > 47 AND unsigned(iv_RXdata) < 58) then
                     vR <= resize( vR * to_unsigned(10, vR'length), vR'length ) + resize(unsigned(iv_RXdata(3 downto 0)), vR'length);
                  else
                     state <= hash;
                  end if;
               when R_fh      =>
                  state <= R_comma;
                  if(unsigned(iv_RXdata) > 47 AND unsigned(iv_RXdata) < 58) then
                     vR <= resize( vR * to_unsigned(10, vR'length), vR'length ) + resize(unsigned(iv_RXdata(3 downto 0)), vR'length);
                  else
                     state <= hash;
                  end if;
               when R_comma   =>
                  --divide by 18000 = multiply by 238609 ( 111010010000010001 ) and divide by 65536 ( right shift 16 bits )
                  --vR <= vR + (vR(27 downto 0) & "0000") + (vR(21 downto 0) & "0000000000") + (vR(18 downto 0) & "0000000000000") + (vR(16 downto 0) & "000000000000000") + (vR(15 downto 0) & "0000000000000000") + (vR(14 downto 0) & "0000000000000000");
                  vR <= shift_right(resize(vR * 238609, vR'length), 16);
                  if(iv_RXdata = "00101100") then
                     state <= P_sign;
                  else
                     state <= hash;
                  end if;
               when P_sign    =>                --Wait for sign of Roll value character. 
                  state <= P_h;
                  if(iv_RXdata = "00101011") then
                     vPp <= '1';
                  elsif(iv_RXdata = "00101101") then
                     vPp <= '0';
                  else
                     state <= hash;
                  end if;  
               when P_h       =>                --Wait for hundreds digit of Roll. 
                  state <= P_t;
                  if(unsigned(iv_RXdata) > 47 AND unsigned(iv_RXdata) < 50) then
                     vP <= resize(unsigned(iv_RXdata(3 downto 0)), vP'length);
                  else
                     state <= hash;
                  end if;
               when P_t       =>                --Wait for tens digit of Roll.
                  state <= P_u;
                  if(unsigned(iv_RXdata) > 47 AND unsigned(iv_RXdata) < 58) then
                     vP <= resize( vP * to_unsigned(10, vP'length), vP'length ) + resize(unsigned(iv_RXdata(3 downto 0)), vP'length);
                  else
                     state <= hash;
                  end if;
               when P_u       =>                --Wait for units digit of Roll.
                  state <= P_dot;
                  if(unsigned(iv_RXdata) > 47 AND unsigned(iv_RXdata) < 58) then
                     vP <= resize( vP * to_unsigned(10, vP'length), vP'length ) + resize(unsigned(iv_RXdata(3 downto 0)), vP'length);
                  else
                     state <= hash;
                  end if;
               when P_dot     =>
                  if(iv_RXdata = "00101110") then
                     state <= P_ft;
                  else
                     state <= hash;
                  end if;
               when P_ft      =>
                  state <= P_fh;
                  if(unsigned(iv_RXdata) > 47 AND unsigned(iv_RXdata) < 58) then
                     vP <= resize( vP * to_unsigned(10, vP'length), vP'length ) + resize(unsigned(iv_RXdata(3 downto 0)), vP'length);
                  else
                     state <= hash;
                  end if;
               when P_fh      =>
                  state <= P_comma;
                  if(unsigned(iv_RXdata) > 47 AND unsigned(iv_RXdata) < 58) then
                     vP <= resize( vP * to_unsigned(10, vP'length), vP'length ) + resize(unsigned(iv_RXdata(3 downto 0)), vP'length);
                  else
                     state <= hash;
                  end if;
               when P_comma   =>
                  --divide by 18000 = multiply by 238609 ( 111010010000010001 ) and divide by 65536 ( right shift 16 bits )
                  --vP <= vP + (vP(27 downto 0) & "0000") + (vP(21 downto 0) & "0000000000") + (vP(18 downto 0) & "0000000000000") + (vP(16 downto 0) & "000000000000000") + (vP(15 downto 0) & "0000000000000000") + (vP(14 downto 0) & "0000000000000000");
                  vP <= shift_right(resize(vP * 238609, vP'length), 16);
                  if(iv_RXdata = "00101100") then
                     state <= Y_sign;
                  else
                     state <= hash;
                  end if;
               when Y_sign    =>                --Wait for sign of Roll value character. 
                  state <= Y_h;
                  if(iv_RXdata = "00101011") then
                     vYp <= '1';
                  elsif(iv_RXdata = "00101101") then
                     vYp <= '0';
                  else
                     state <= hash;
                  end if;
                     
               when Y_h       =>                --Wait for hundreds digit of Roll. 
                  state <= Y_t;
                  if(unsigned(iv_RXdata) > 47 AND unsigned(iv_RXdata) < 50) then
                     vY <= resize(unsigned(iv_RXdata(3 downto 0)), vY'length);
                  else
                     state <= hash;
                  end if;
               when Y_t       =>                --Wait for tens digit of Roll.
                  state <= Y_u;
                  if(unsigned(iv_RXdata) > 47 AND unsigned(iv_RXdata) < 58) then
                     vY <= resize( vY * to_unsigned(10, vY'length), vY'length ) + resize(unsigned(iv_RXdata(3 downto 0)), vY'length);
                  else
                     state <= hash;
                  end if;
               when Y_u       =>                --Wait for units digit of Roll.
                  state <= Y_dot;
                  if(unsigned(iv_RXdata) > 47 AND unsigned(iv_RXdata) < 58) then
                     vY <= resize( vY * to_unsigned(10, vY'length), vY'length ) + resize(unsigned(iv_RXdata(3 downto 0)), vY'length);
                  else
                     state <= hash;
                  end if;
               when Y_dot     =>
                  if(iv_RXdata = "00101110") then
                     state <= Y_ft;
                  else
                     state <= hash;
                  end if;
               when Y_ft      =>
                  state <= Y_fh;
                  if(unsigned(iv_RXdata) > 47 AND unsigned(iv_RXdata) < 58) then
                     vY <= resize( vY * to_unsigned(10, vY'length), vY'length ) + resize(unsigned(iv_RXdata(3 downto 0)), vY'length);
                  else
                     state <= hash;
                  end if;
               when Y_fh      =>
                  state <= Y_comma;
                  if(unsigned(iv_RXdata) > 47 AND unsigned(iv_RXdata) < 58) then
                     vY <= resize( vY * to_unsigned(10, vY'length), vY'length ) + resize(unsigned(iv_RXdata(3 downto 0)), vY'length);
                  else
                     state <= hash;
                  end if;
               when Y_comma   =>
                  --divide by 18000 = multiply by 238609 ( 111010010000010001 ) and divide by 65536 ( right shift 16 bits )
                  --vY <= vY + (vY(27 downto 0) & "0000") + (vY(21 downto 0) & "0000000000") + (vY(18 downto 0) & "0000000000000") + (vY(16 downto 0) & "000000000000000") + (vY(15 downto 0) & "0000000000000000") + (vY(14 downto 0) & "0000000000000000");
                  vY <= shift_right(resize(vY * 238609, vY'length), 16);
                  if(iv_RXdata = "00101100") then
                     state <= ZD;
                  else
                     state <= hash;
                  end if;
               when ZD        =>
                  if(iv_RXdata = "00001101") then
                     state <= ZA;
                  else
                     state <= hash;
                  end if;
               when ZA        =>
                  state <= hash;
                  if(iv_RXdata = "00001010") then
                     if(vYp = '1') then
                        yaw <= signed(vY);
                     else
                        yaw <= to_signed(0, 32) - signed(vY);
                     end if;
                     
                     if(vPp = '1') then -- tutaj celowa zamiana!
                        pitch <= to_signed(0, 32) - signed(vP);
                     else
                        pitch <= signed(vP);
                     end if;
                     
                     if(vRp = '1') then
                        roll <= signed(vR);
                     else
                        roll <= to_signed(0, 32) - signed(vR);
                     end if;
                     DRDY <= '1';
                  end if;
                  
               when others =>
                  DRDY  <= '0';
                  yaw   <= (others => '0');
                  pitch <= (others => '0');
                  roll  <= (others => '0');
                  state <= hash;
            end case;
         end if;
      end if;
   end process state_machine;
   
   
end bhv;