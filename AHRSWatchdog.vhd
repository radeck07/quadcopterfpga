--Name:     UART_TX
--Author:   Radoslaw Kalinka
--Date:     2016.10.08
--Desc:     AHRS Watchdog module. Restarts AHRS commander after timeout. 
--          Module works with 50MHz input clock
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.ALL;

entity AHRSWatchdog is
   PORT(
      i_clk                :  IN std_logic;                          --System Clock Input
      i_rst                :  IN std_logic;
      i_tick               :  IN std_logic;                          --Tick from AHRSDataRcv - high state resets watchdog
      o_resetAHRSCommander :  OUT std_logic                          --Reset AHRSCommander   
   );
end;

architecture bhv of AHRSWatchdog is
   signal timer         :  unsigned(31 DOWNTO 0) := (others => '0'); --watchdog timer
begin
   process(i_clk, i_rst) 
   begin
      if(i_rst = '1') then
         o_resetAHRSCommander       <= '0';
         timer                      <= (others => '0');
      elsif(rising_edge(i_clk)) then
         o_resetAHRSCommander       <= '0';
         if(i_tick = '1') then
            timer                   <= (others => '0');
         else
            if(timer < 1000000) then                                  --timeout = 20ms
               timer                <= timer + 1;
            else
               o_resetAHRSCommander <= '1';
               timer                <= (others => '0');
            end if;
         end if;
      end if;
   end process;
end bhv;