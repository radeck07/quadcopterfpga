library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.ALL;

entity RCWatchdog is
	PORT(
		clk						:	IN	std_logic;								--System Clock Input
		tick						:	IN	std_logic;								--Tick from RCDataRcv - low state resets watchdog
		emergencyThrottleIn	:	IN std_logic_vector(9 downto 0);		--Throttle value from safe altitude PID regulator (not implemented yet)
		throttleIn				:	IN std_logic_vector(9 downto 0);		--Throttle value from RCDataRcv
		yawIn						:	IN std_logic_vector(9 downto 0);		--Yaw value from RCDataRcv
		pitchIn					:	IN std_logic_vector(9 downto 0);		--Pitch value from RCDataRcv
		rollIn					:	IN std_logic_vector(9 downto 0);		--Roll value from RCDataRcv
		throttleOut				:	OUT std_logic_vector(9 downto 0);	--Throttle value output. Equals throttleIn when watchdog is reset before timeout. Else it equals emergencyThrottleIn.
		yawOut					:	OUT std_logic_vector(9 downto 0);	--Yaw value output. Equals YawIn when watchdog is reset before timeout. Else it equals 0.
		pitchOut					:	OUT std_logic_vector(9 downto 0);	--Pitch value output. Equals PitchIn when watchdog is reset before timeout. Else it equals 0.
		rollOut					:	OUT std_logic_vector(9 downto 0)		--Roll value output. Equals RollIn when watchdog is reset before timeout. Else it equals 0.
	);
end;

architecture bhv of RCWatchdog is
	signal timer			:	unsigned(31 DOWNTO 0) := (others => '0');	--watchdog timer
	
begin
	process(clk, tick, throttleIn, yawIn, pitchIn, rollIn) 
	variable emergencyOn	:	std_logic := '1';									--emergency switch. 1 - emergency on, 0 - emergency off
	begin
		if(falling_edge(clk)) then
			throttleOut <= throttleIn;
			yawOut <= yawIn;
			pitchOut <= pitchIn;
			rollOut <= rollIn;
			if(tick = '0') then
				timer <= (others => '0');
				emergencyOn := '0';
			else
				if(timer < to_integer(to_unsigned(2500000, 32))) then	--timeout = 100ms
					timer <= timer + 1;
				else
					emergencyOn := '1';
					timer <= (others => '0');
				end if;
			end if;
			if(emergencyOn = '1') then
				throttleOut <= emergencyThrottleIn;
				yawOut <= std_logic_vector(to_unsigned(500, 10));
				pitchOut <= std_logic_vector(to_unsigned(500, 10));
				rollOut <= std_logic_vector(to_unsigned(500, 10));
			end if;
		end if;
	end process;
end bhv;