--Name:     UART_RX_tb
--Author:   Radoslaw Kalinka
--Date:     2017.03.08
--Desc:     Uart receiver module testbench
--          Module works with 50MHz input clock
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.ALL; 

entity UART_RX_tb is
end;

architecture bhv of UART_RX_tb is
   component UART_RX is
      GENERIC(
         G_BIT_LENGTH     :integer := 434                      --Bit length in clk tacts
      );
      PORT(
         i_clk       : IN    std_logic;                        --input clk
         i_rst       : IN    std_logic;                        --reset signal
         i_RX        : IN    std_logic;                        --data input
         o_DRDY      : OUT   std_logic;                        --data ready
         ov_RXDATA   : OUT   std_logic_vector(7 downto 0)
      );
   end component UART_RX;
   
   signal clk       : std_logic;                  
   signal rst       : std_logic;                  
   signal RX        : std_logic;                  
   signal DRDY      : std_logic;                  
   signal RXDATA    : std_logic_vector(7 downto 0);
   
begin

   UART_RX_inst: UART_RX   generic map(  G_BIT_LENGTH => 434 )
                           port map(
                           i_clk       => clk,
                           i_rst       => rst,
                           i_RX        => RX,
                           o_DRDY      => DRDY,
                           ov_RXDATA   => RXDATA
                           );
   
   clk_gen: process                
   begin                           
      clk <= '0';                 
      wait for 10 ns;             
      clk <= '1';                 
      wait for 10 ns;
   end process;
   
   stim_process: process
   begin
      rst <= '1';
      RX  <= '1';
      wait for 20 ns;
      rst <= '0';
      wait for 20 ns;
      RX  <= '1';
      wait for 434*20 ns;
      --start
      RX  <= '0';
      wait for 434*20 ns;
      
      RX  <= '1';
      wait for 434*20 ns;
      RX  <= '0';
      wait for 434*20 ns;
      RX  <= '1';
      wait for 434*20 ns;
      RX  <= '1';
      wait for 434*20 ns;
      RX  <= '0';
      wait for 434*20 ns;
      RX  <= '1';
      wait for 434*20 ns;
      RX  <= '0';
      wait for 434*20 ns;
      RX  <= '0';
      wait for 434*20 ns;
      
      --end
      RX  <= '1';
      wait for 434*20 ns;
      
      --start
      RX  <= '0';
      wait for 434*20 ns;
      
      RX  <= '1';
      wait for 434*20 ns;
      RX  <= '1';
      wait for 434*20 ns;
      RX  <= '1';
      wait for 434*20 ns;
      RX  <= '1';
      wait for 434*20 ns;
      RX  <= '1';
      wait for 434*20 ns;
      RX  <= '1';
      wait for 434*20 ns;
      RX  <= '1';
      wait for 434*20 ns;
      RX  <= '1';
      wait for 434*20 ns;
      
      --end
      RX  <= '1';
      wait for 434*20 ns;
      
      --start
      RX  <= '0';
      wait for 434*20 ns;
      
      RX  <= '0';
      wait for 434*20 ns;
      RX  <= '0';
      wait for 434*20 ns;
      RX  <= '0';
      wait for 434*20 ns;
      RX  <= '0';
      wait for 434*20 ns;
      RX  <= '0';
      wait for 434*20 ns;
      RX  <= '0';
      wait for 434*20 ns;
      RX  <= '0';
      wait for 434*20 ns;
      RX  <= '0';
      wait for 434*20 ns;
      
      --end
      RX  <= '1';
      wait for 434*20 ns;
      
      --start
      RX  <= '0';
      wait for 434*20 ns;
      
      RX  <= '1';
      wait for 434*20 ns;
      RX  <= '0';
      wait for 434*20 ns;
      RX  <= '1';
      wait for 434*20 ns;
      RX  <= '0';
      wait for 434*20 ns;
      RX  <= '1';
      wait for 434*20 ns;
      RX  <= '0';
      wait for 434*20 ns;
      RX  <= '1';
      wait for 434*20 ns;
      RX  <= '0';
      wait for 434*20 ns;
      
      --end
      RX  <= '1';
      wait for 434*20 ns;
      
      --start
      RX  <= '0';
      wait for 434*20 ns;
      
      RX  <= '1';
      wait for 434*20 ns;
      RX  <= '0';
      wait for 434*20 ns;
      RX  <= '1';
      wait for 434*20 ns;
      RX  <= '1';
      wait for 434*20 ns;
      RX  <= '0';
      wait for 434*20 ns;
      RX  <= '1';
      wait for 434*20 ns;
      RX  <= '0';
      wait for 434*20 ns;
      RX  <= '0';
      wait for 434*20 ns;
      
      --end
      RX  <= '1';
      wait for 434*20 ns;
      
      --start
      RX  <= '0';
      wait for 434*20 ns;
      
      RX  <= '1';
      wait for 434*20 ns;
      RX  <= '1';
      wait for 434*20 ns;
      RX  <= '1';
      wait for 434*20 ns;
      RX  <= '1';
      wait for 434*20 ns;
      RX  <= '1';
      wait for 434*20 ns;
      RX  <= '1';
      wait for 434*20 ns;
      RX  <= '1';
      wait for 434*20 ns;
      RX  <= '1';
      wait for 434*20 ns;
      
      --end
      RX  <= '1';
      wait for 434*20 ns;
      
      --start
      RX  <= '0';
      wait for 434*20 ns;
      
      RX  <= '0';
      wait for 434*20 ns;
      RX  <= '0';
      wait for 434*20 ns;
      RX  <= '0';
      wait for 434*20 ns;
      RX  <= '0';
      wait for 434*20 ns;
      RX  <= '0';
      wait for 434*20 ns;
      RX  <= '0';
      wait for 434*20 ns;
      RX  <= '0';
      wait for 434*20 ns;
      RX  <= '0';
      wait for 434*20 ns;
      
      --end
      RX  <= '1';
      wait for 434*20 ns;
      
      --start
      RX  <= '0';
      wait for 434*20 ns;
      
      RX  <= '1';
      wait for 434*20 ns;
      RX  <= '0';
      wait for 434*20 ns;
      RX  <= '1';
      wait for 434*20 ns;
      RX  <= '0';
      wait for 434*20 ns;
      RX  <= '1';
      wait for 434*20 ns;
      RX  <= '0';
      wait for 434*20 ns;
      RX  <= '1';
      wait for 434*20 ns;
      RX  <= '0';
      wait for 434*20 ns;
      
      --end
      RX  <= '1';
      wait for 434*20 ns;
      
      wait;
   end process;
   
end bhv;

