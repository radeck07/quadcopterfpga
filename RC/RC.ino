#define  THROTTLE  A0
#define  YAW       A1
#define  PITCH     A2
#define  ROLL      A3
#define  L_KEYS    A4
#define  R_KEYS    A5
#define  BATT      A6
uint8_t buff[9];
uint16_t throttle, yaw, pitch, roll;
unsigned long time_;
double T = 512.0, Y = 512.0, P = 512.0, R = 512.0;
double maxT = 0.0, minT = 2000.0, maxY = 0.0, minY = 2000.0, maxP = 0.0, minP = 2000.0, maxR = 0.0, minR = 2000.0;
bool thrCal = false, yawCal = false, pitCal = false, rolCal = false, calibrated = false;

void setup(){
  Serial.begin(9600);
  analogReference(EXTERNAL);
  time_ = millis();
}

void sendToRadio(){
  
  buff[0] = 0;
  buff[1] = 1 << 5;
  buff[2] = 2 << 5;
  buff[3] = 3 << 5;
  buff[4] = 4 << 5;
  buff[5] = 5 << 5;
  buff[6] = 6 << 5;
  buff[7] = 7 << 5;
  buff[8] = 0;
  
  buff[0] |= (uint8_t)((throttle >> 5) & 0x1F);
  buff[1] |= (uint8_t)(throttle & 0x1F);
  buff[2] |= (uint8_t)((yaw >> 5) & 0x1F);
  buff[3] |= (uint8_t)(yaw & 0x1F);
  buff[4] |= (uint8_t)((pitch >> 5)  & 0x1F);
  buff[5] |= (uint8_t)(pitch & 0x1F);
  buff[6] |= (uint8_t)((roll >> 5) & 0x1F);
  buff[7] |= (uint8_t)(roll & 0x1F);
  buff[8] = (uint8_t)((throttle >> 2)&0xFF);                    //T9 T8 T7 T6 T5 T4 T3 T2
  buff[8] ^= (uint8_t)((throttle << 6)&0xC0 | (yaw >> 4)&0x3F); //T1 T0 Y9 Y8 Y7 Y6 Y5 Y4
  buff[8] ^= (uint8_t)((yaw << 4)&0xF0 | (pitch >> 6)&0x0F);    //Y3 Y2 Y1 Y0 P9 P8 P7 P6
  buff[8] ^= (uint8_t)((pitch << 2)&0xFC | (roll >> 8)&0x03);   //P5 P4 P3 P2 P1 P0 R9 R8
  buff[8] ^= (uint8_t)((roll)&0xFF);                            //R7 R6 R5 R4 R3 R2 R1 R0
  
  Serial.write(buff, 9);
}

bool isMidVal(uint16_t val){
  return val>450 && val <550;
}

bool isMinVal(uint16_t val){
  return val<50;
}

void loop(){
  uint16_t t,y,p,r;
  
  if(!calibrated){
    T = 0.90*T + 0.10*(double)(analogRead(A0));
    Y = 0.90*Y + 0.10*(double)(analogRead(A1));
    P = 0.90*P + 0.10*(double)(analogRead(A2));
    R = 0.90*R + 0.10*(double)(analogRead(A3));
    if(maxT<T)
      maxT = T;
    if(minT>T)
      minT = T;
    if(maxY<Y)
      maxY = Y;
    if(minY>Y)
      minY = Y;
    if(maxP<P)
      maxP = P;
    if(minP>P)
      minP = P;
    if(maxR<R)
      maxR = R;
    if(minR>R)
      minR = R;
  }
  
  if(millis() - time_ >= 20){
    time_ = millis();
    
    if(calibrated){
      t = analogRead(A0);
      y = analogRead(A1);
      p = analogRead(A2);
      r = analogRead(A3);

      if(maxT<(double)t)
        t = (uint16_t)maxT;
      if(minT>(double)t)
        t = (uint16_t)minT;
      if(maxY<(double)y)
        y = (uint16_t)maxY;
      if(minY>(double)y)
        y = (uint16_t)minY;
      if(maxP<(double)p)
        p = (uint16_t)maxP;
      if(minP>(double)p)
        p = (uint16_t)minP;
      if(maxR<(double)r)
        r = (uint16_t)maxR;
      if(minR>(double)r)
        r = (uint16_t)minR;
    }

    if((maxT != minT) && (maxY != minY) && (maxP != minP) && (maxR != minR)){
      throttle  = (uint16_t)(0.5 + (((double)t - minT)/(maxT-minT)) * 1023.0);
      yaw       = (uint16_t)(0.5 + (((double)y - minY)/(maxY-minY)) * 1023.0);
      pitch     = (uint16_t)(0.5 + (((double)p - minP)/(maxP-minP)) * 1023.0);
      roll      = (uint16_t)(0.5 + (((double)r - minR)/(maxR-minR)) * 1023.0);
    }
    
    if(!calibrated)
      if(thrCal && yawCal && pitCal && rolCal && isMinVal(throttle) && isMidVal(yaw) && isMidVal(pitch) && isMidVal(roll)){
        calibrated = true;
      }else{
        if(maxT > 900.0 && minT < 100.0)
          thrCal = true;

        if(maxY > 900.0 && minY < 100.0)
          yawCal = true;

        if(maxP > 900.0 && minP < 100.0)
          pitCal = true;

        if(maxR > 900.0 && minR < 100.0)
          rolCal = true;
      }
    
    if(calibrated)
      sendToRadio();
  }
}
