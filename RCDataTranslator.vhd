library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.ALL; 

entity RCDataTranslator is
	PORT(  
		thrIn		: IN	std_logic_vector(9 downto 0);
		yawIn		: IN	std_logic_vector(9 downto 0);
		pitchIn	: IN	std_logic_vector(9 downto 0);
		rollIn	: IN	std_logic_vector(9 downto 0);
		
		thrOut	: OUT	signed(31 downto 0);
		yawOut	: OUT	signed(31 downto 0);
		pitchOut	: OUT	signed(31 downto 0);
		rollOut	: OUT	signed(31 downto 0)
	);
end;

architecture bhv of RCDataTranslator is
begin
	thrOut(31 downto 16)	<=	"0000000000000000";
	thrOut(15 downto 6)	<=	signed(thrIn);
	thrOut(5 downto 0)	<= "000000" when thrIn(0) = '0' else "111111";
	
	yawOut(31 downto 14)	<=	"000000000000000000" when yawIn(9) = '1' else "111111111111111111";
	yawOut(13 downto 5)	<=	signed(yawIn(8 downto 0));
	yawOut(4 downto 0)	<=	"00000" when yawIn(0) = '0' else "11111";
	
	pitchOut(31 downto 14)	<=	"000000000000000000" when pitchIn(9) = '1' else "111111111111111111";
	pitchOut(13 downto 5)	<=	signed(pitchIn(8 downto 0));
	pitchOut(4 downto 0)	<=	"00000" when pitchIn(0) = '0' else "11111";
	
	rollOut(31 downto 14)	<=	"000000000000000000" when rollIn(9) = '1' else "111111111111111111";
	rollOut(13 downto 5)	<=	signed(rollIn(8 downto 0));
	rollOut(4 downto 0)	<=	"00000" when rollIn(0) = '0' else "11111";
	
end bhv;