--Name:     SPIMaster
--Author:   Radoslaw Kalinka
--Date:     2017.06.04
--Desc:     SPI module
--          Module works with 50MHz input clock
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.ALL;
library work;

entity SPI_Master is
   GENERIC(
      G_BIT_LENGTH      : integer := 434;                      --Bit length in clk tacts
      G_CPOL            : std_logic := '0';                        --CPOL
      G_CPHA            : std_logic := '0';                        --CPHA
      G_BufferLen       : integer := 8
   );
   PORT(
      i_clk                : IN std_logic;                        --input clk
      i_rst                : IN std_logic;                        --reset signal
      i_tick               : IN std_logic;
      iv_BitsToSend        : IN std_logic_vector(G_BufferLen-1 downto 0);
      i_MISO               : IN std_logic;
      o_MOSI               : OUT std_logic;
      o_SS                 : OUT std_logic;
      ov_ReceivedBits      : OUT std_logic_vector(G_BufferLen-1 downto 0);
      o_DRDY               : OUT std_logic
   );
end; 

architecture bhv of SPI_Master is
begin
   
end;