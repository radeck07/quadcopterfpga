--Name:     MPU9250Kalman
--Author:   Radoslaw Kalinka
--Date:     2017.06.04
--Desc:     MPU9250 Driver Module 
--          Module works with 50MHz input clock
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_misc.all;
use ieee.numeric_std.ALL;
library work;
use work.regs.all;

entity MPU9250Kalman is
   PORT(
         i_clk       : IN  STD_LOGIC;
         i_clk5M     : IN  STD_LOGIC;
         i_rst       : IN  STD_LOGIC;
         iv_DataIn   : IN  std_logic_vector(111 downto 0);
         i_SPIBsy    : IN  std_logic;
         i_SPIDrdy   : IN  std_logic;
         ov_DataOut  : OUT std_logic_vector(15 downto 0);
         ov_BitLen   : OUT std_logic_vector(7 downto 0);
         ov_DataLen  : OUT std_logic_vector(7 downto 0);
         o_SPIStart  : OUT std_logic;
         ov_angx     : OUT s2q16;
         ov_angy     : OUT s2q16;
         ov_angz     : OUT s2q16;
         o_DRDY      : OUT std_logic;                               --Goes low at new valid data recognition
         ov_romtest_a  : OUT std_logic_vector(13 downto 0);
         ov_romtest_b  : OUT std_logic_vector(13 downto 0);
         ov_sqrt_q   : OUT std_logic_vector(47 downto 0)
      );
end;

architecture bhv of MPU9250Kalman is
   attribute keep: boolean;
   constant Q_angle     : signed (31 downto 0) := to_signed(66, 32);       --0.001
   constant Q_bias      : signed (31 downto 0) := to_signed(197, 32);      --0.003
   constant R_measure   : signed (31 downto 0) := to_signed(1966, 32);     --0.03
   type state_type is (who_am_I_ask, mpu9250_retry,  disI2C, enable_sensors, wait_for_wakeup, clock_select, wait_for_clock, config, gyro_config, acc1_config, acc2_config, MPU_read, AccXSqare, AccYSqare, AccZSqare, AccVectNormSum, AccVectNormX, AccVectNormY, AccVectNormZ, AccXSqare2, AccYSqare2, AccZSqare2, AccYAngCalc1, AccYAngCalc2, AccXAngCalc, AccYRadToDeg, AccXRadToDeg, KalmanXst0, KalmanXst1, KalmanXst2, KalmanXst3, KalmanXst4, KalmanXst5, KalmanXst6, KalmanXst7, KalmanXst8, KalmanXst9, KalmanXst10, KalmanXst11, KalmanXst12, KalmanXst13, KalmanXst14, KalmanXst15, KalmanYst0, KalmanYst1, KalmanYst2, KalmanYst3, KalmanYst4, KalmanYst5, KalmanYst6, KalmanYst7, KalmanYst8, KalmanYst9, KalmanYst10, KalmanYst11, KalmanYst12, KalmanYst13, KalmanYst14, KalmanYst15, SensorXOut, SensorYOut, SensorZOut, wait_for_new_data);
   signal state                     : state_type := who_am_I_ask;
   signal cnt                       : integer range 0 to 10000000;
   signal cntpr                     : integer range 0 to 10000000;
   signal cntoff                    : integer range 0 to 1024;
   signal DataOut                   : std_logic_vector(15 downto 0);
   signal BitLen                    : std_logic_vector(7 downto 0);
   signal DataLen                   : std_logic_vector(7 downto 0);
   signal SPIStart                  : std_logic;
   signal accx, accy, accz          : s0q16;
   signal gyrx, gyry, gyrz          : s0q16;
   signal accx2, accy2, accz2       : s16q16;
   signal accxang, accyang          : s16q16;
   signal accxang_prv, accyang_prv  : s16q16;
   signal angx_o, angy_o, angz_o    : s2q16;
   signal gyrxoff, gyryoff, gyrzoff : s0q16;
   signal angz                      : s16q16;
   signal sqrt_radical              : std_logic_vector(33 downto 0);
   signal sqrt_q                    : std_logic_vector(16 downto 0);
	signal sqrt_tick						: std_logic;
	signal sqrt_BSY                  : std_logic;
	signal sqrt_DRDY                 : std_logic;
   signal atan2_q                   : std_logic_vector(12 downto 0);
   signal atan2_x                   : s1q16;
   signal atan2_y                   : s1q16;
   signal DRDY                      : std_logic;
   signal K_angle_x                 : signed(31 downto 0);
   signal K_bias_x                  : signed(31 downto 0);
   signal P00_x                     : signed(31 downto 0);
   signal P01_x                     : signed(31 downto 0);
   signal P10_x                     : signed(31 downto 0);
   signal P11_x                     : signed(31 downto 0);
   signal S_x                       : signed(31 downto 0);
   signal K0_x                      : signed(31 downto 0);
   signal K1_x                      : signed(31 downto 0);
   signal y_x                       : signed(31 downto 0);
   signal K_angle_y                 : signed(31 downto 0);
   signal K_bias_y                  : signed(31 downto 0);
   signal P00_y                     : signed(31 downto 0);
   signal P01_y                     : signed(31 downto 0);
   signal P10_y                     : signed(31 downto 0);
   signal P11_y                     : signed(31 downto 0);
   signal S_y                       : signed(31 downto 0);
   signal K0_y                      : signed(31 downto 0);
   signal K1_y                      : signed(31 downto 0);
   signal y_y                       : signed(31 downto 0);
   signal denom                     : std_logic_vector(31 downto 0);
   signal numer                     : std_logic_vector(47 downto 0);
   signal quotient                  : std_logic_vector(47 downto 0);
   signal atan_rom_addr_a           : std_logic_vector(13 downto 0);
   signal atan_rom_q_a              : std_logic_vector(13 downto 0);
   signal atan_rom_addr_b           : std_logic_vector(13 downto 0);
   signal atan_rom_q_b              : std_logic_vector(13 downto 0);
   signal div_tick                  : std_logic;
   signal div_numer                 : std_logic_vector(47 downto 0);
   signal div_denom                 : std_logic_vector(32 downto 0);
   signal div_result                : std_logic_vector(47 downto 0);
   signal div_drdy                  : std_logic;
   signal div_bsy                   : std_logic;
   
   signal ATAN_rom : ATAN_memory_t := ATAN_init_rom;
   
begin

sqrt_inst: entity work.Sqrt(bhv)
   PORT MAP(
      i_clk       => i_clk,
      i_rst       => i_rst,
      i_tick      => sqrt_tick,
      iv_input    => sqrt_radical,
      ov_sqrt     => sqrt_q,
      o_DRDY      => sqrt_DRDY,
      o_BSY       => sqrt_BSY
   );
   
   Divider_inst: entity work.Divider(bhv)
   PORT MAP(
      i_clk       => i_clk,
      i_rst       => i_rst,
      i_tick      => div_tick,
      iv_Numer    => div_numer,
      iv_Denomin  => div_denom,
      ov_Result   => div_result,
      o_DRDY      => div_drdy,
      o_BSY       => div_bsy
   );
   
   atan_rom_inst: entity work.atan_rom(rtl)
   generic map
   (
      DATA_WIDTH => 14,
      ADDR_WIDTH => 14
   )
   port map
   (
      clk    => i_clk,
      addr   => to_integer(unsigned(atan_rom_addr_a)),
      q      => atan_rom_q_a
   );
   
   atan_rom_addr_a <= std_logic_vector(accx(13 downto 0));
   ov_romtest_a <= atan_rom_q_a;

   ov_DataOut  <= DataOut;
   ov_BitLen   <= BitLen;
   ov_DataLen  <= DataLen;
   o_SPIStart  <= SPIStart;
   ov_angx     <= angx_o;
   ov_angy     <= angy_o;
   ov_angz     <= angz_o;
   o_DRDY      <= DRDY;
   
   process(i_clk, i_rst)
   begin
      if(i_rst = '1') then
         state          <= who_am_I_ask;
         cnt            <= 0;
         cntpr          <= 0;
         cntoff         <= 0;
         DataOut        <= (others => '0');
         BitLen         <= (others => '0');
         DataLen        <= (others => '0');
         SPIStart       <= '0';
         accx           <= (others => '0');
         accy           <= (others => '0');
         accz           <= (others => '0');
         accx2          <= (others => '0');
         accy2          <= (others => '0');
         accz2          <= (others => '0');
         accxang        <= (others => '0');
         accyang        <= (others => '0');
         accxang_prv    <= (others => '0');
         accyang_prv    <= (others => '0');
         gyrx           <= (others => '0');
         gyry           <= (others => '0');
         gyrz           <= (others => '0');
         angx_o         <= (others => '0');
         angy_o         <= (others => '0');
         angz_o         <= (others => '0');
         gyrxoff        <= (others => '0');
         gyryoff        <= (others => '0');
         gyrzoff        <= (others => '0');
         div_tick       <= '0';
         div_numer      <= (others => '0');
         div_denom      <= (others => '0');
         sqrt_radical   <= (others => '0');
			sqrt_tick		<= '0';
         atan2_x        <= (others => '0');
         atan2_y        <= (others => '0');
         K_angle_x      <= (others => '0');
         K_bias_x       <= (others => '0');
         P00_x          <= (others => '0');
         P01_x          <= (others => '0');
         P10_x          <= (others => '0');
         P11_x          <= (others => '0');
         S_x            <= (others => '0');
         K0_x           <= (others => '0');
         K1_x           <= (others => '0');
         y_x            <= (others => '0');
         K_angle_y      <= (others => '0');
         K_bias_y       <= (others => '0');
         P00_y          <= (others => '0');
         P01_y          <= (others => '0');
         P10_y          <= (others => '0');
         P11_y          <= (others => '0');
         S_y            <= (others => '0');
         K0_y           <= (others => '0');
         K1_y           <= (others => '0');
         y_y            <= (others => '0');
         angz           <= (others => '0');
         DRDY           <= '0';
      elsif(rising_edge(i_clk)) then
         case state is
            when who_am_I_ask =>
               DataOut  <= "1111010100000000";  --who_am_I(117) read
               DataLen  <= "00010000";          --16bit transaction
               BitLen   <= "00110010";          --1MHz (1/50)
               DRDY     <= '0';
               
               if(i_SPIDrdy = '0') then
                  if(i_SPIBsy = '0') then
                     SPIStart <= '1';
                  else
                     SPIStart <= '0';
                  end if;
               else
                  if(iv_DataIn(7 downto 0) = "01110001") then
                     state <= disI2C;
                  else
                     state <= mpu9250_retry;
                  end if;
                  cnt      <= 0;
               end if;
               
            when mpu9250_retry =>
               if(cnt < 50000) then
                  cnt <= cnt + 1;
               else
                  state <= who_am_I_ask;
               end if;
               
            when disI2C =>
               DataOut  <= "0110101000010000";  --User Control 106 I2C Disable bit[4]
               DataLen  <= "00010000";          --16bit transaction
               BitLen   <= "00110010";          --1MHz (1/50)
               
               if(i_SPIDrdy = '0') then
                  if(i_SPIBsy = '0') then
                     SPIStart <= '1';
                  else
                     SPIStart <= '0';
                  end if;
               else
                  --DataIn   <= iv_DataIn;
                  state    <= enable_sensors;
                  cnt      <= 0;
               end if;
               
            when enable_sensors =>
               DataOut  <= "0110101100000000";  --Power Mgmt 107 Clear sleep mode bit[6] enable all sensors
               DataLen  <= "00010000";          --16bit transaction
               BitLen   <= "00110010";          --1MHz (1/50)
               
               if(i_SPIDrdy = '0') then
                  if(i_SPIBsy = '0') then
                     SPIStart <= '1';
                  else
                     SPIStart <= '0';
                  end if;
               else
                  --DataIn   <= iv_DataIn;
                  state    <= wait_for_wakeup;
                  cnt      <= 0;
               end if;
               
            when wait_for_wakeup =>
               if(cnt < 5000000) then
                  cnt <= cnt + 1;
               else
                  state <= clock_select;
               end if;
               
            when clock_select =>
               DataOut  <= "0110101100000001";  --Power Mgmt 107 Auto selects the best available clock source â€“ PLL if ready, else use the Internal oscillator
               DataLen  <= "00010000";          --16bit transaction
               BitLen   <= "00110010";          --1MHz (1/50)
               
               if(i_SPIDrdy = '0') then
                  if(i_SPIBsy = '0') then
                     SPIStart <= '1';
                  else
                     SPIStart <= '0';
                  end if;
               else
                  --DataIn   <= iv_DataIn;
                  state    <= wait_for_clock;
                  cnt      <= 0;
               end if;
            
            when wait_for_clock =>
               if(cnt < 10000000) then
                  cnt <= cnt + 1;
               else
                  state <= config;
               end if;
            
            when config =>
               DataOut  <= "0001101000000000";  --Config 26 FIFO off [0], DLPF 0 [2-0]
               DataLen  <= "00010000";          --16bit transaction
               BitLen   <= "00110010";          --1MHz (1/50)
               
               if(i_SPIDrdy = '0') then
                  if(i_SPIBsy = '0') then
                     SPIStart <= '1';
                  else
                     SPIStart <= '0';
                  end if;
               else
                  --DataIn   <= iv_DataIn;
                  state    <= gyro_config;
                  cnt      <= 0;
               end if;
               
            when gyro_config =>
               DataOut  <= "0001101100011010";  --Gyro_Config 27 Fs=32kHz, Bandwidth=3600Hz, 
               DataLen  <= "00010000";          --16bit transaction
               BitLen   <= "00110010";          --1MHz (1/50)
               
               if(i_SPIDrdy = '0') then
                  if(i_SPIBsy = '0') then
                     SPIStart <= '1';
                  else
                     SPIStart <= '0';
                  end if;
               else
                  --DataIn   <= iv_DataIn;
                  state    <= acc1_config;
                  cnt      <= 0;
               end if;
               
            when acc1_config =>
               DataOut  <= "0001110000011000";  --Acc_Config1 28 FullScale 16g [3,4] 
               DataLen  <= "00010000";          --16bit transaction
               BitLen   <= "00110010";          --1MHz (1/50)
               
               if(i_SPIDrdy = '0') then
                  if(i_SPIBsy = '0') then
                     SPIStart <= '1';
                  else
                     SPIStart <= '0';
                  end if;
               else
                  --DataIn   <= iv_DataIn;
                  state    <= acc2_config;
                  cnt      <= 0;
               end if;
               
            when acc2_config =>
               DataOut  <= "0001110100001000";  --Acc_Config2 29 FullScale ODR=1kHz, DLPF=10.2Hz
               DataLen  <= "00010000";          --16bit transaction
               BitLen   <= "00110010";          --1MHz (1/50)
               
               if(i_SPIDrdy = '0') then
                  if(i_SPIBsy = '0') then
                     SPIStart <= '1';
                  else
                     SPIStart <= '0';
                  end if;
               else
                  --DataIn   <= iv_DataIn;
                  state    <= MPU_read;
                  cnt      <= 0;
               end if;
               
            when MPU_read =>
               DataOut  <= "1011101100000000";  --Acc Data + read(0x80)
               DataLen  <= "01111000";          --56 (3x16 + 1x16 + 3x16 + 8)bit transaction
               BitLen   <= "00000100";          --12.5MHz (1/4)
               DRDY     <= '0';
               
               if(i_SPIDrdy = '0') then
                  if(i_SPIBsy = '0') then
                     SPIStart <= '1';
                  else
                     SPIStart <= '0';
                  end if;
               else
                  accx  <= signed(iv_DataIn(111 downto 96));
                  accy  <= signed(iv_DataIn(95  downto 80));
                  accz  <= signed(iv_DataIn(79  downto 64));
                  --temp <= signed(iv_DataIn(63 downto 48));
                  
                  if(cntoff < 1024) then
                     gyrxoff <= gyrxoff + resize(signed(iv_DataIn(47 downto 32)), gyrxoff'length);
                     gyryoff <= gyryoff + resize(signed(iv_DataIn(31 downto 16)), gyryoff'length);
                     gyrzoff <= gyrzoff + resize(signed(iv_DataIn(15 downto  0)), gyrzoff'length);
                     cntoff  <= cntoff + 1;
                     state   <= wait_for_new_data;
                  else
                     gyrx  <= resize(signed(iv_DataIn(47 downto 32)), gyrx'length) - shift_right(gyrxoff, 10);
                     gyry  <= resize(signed(iv_DataIn(31 downto 16)), gyry'length) - shift_right(gyryoff, 10);
                     gyrz  <= resize(signed(iv_DataIn(15 downto  0)), gyrz'length) - shift_right(gyrzoff, 10);
                     angz  <= angz + resize(signed(iv_DataIn(15 downto  0)), angz'length) - shift_right(gyrzoff, 10);
                     state <= AccXSqare;
                  end if;
                  
--                  cnt <= 0;
               end if;
               cnt <= cnt + 1;
               
            when AccXSqare =>
               accx2 <= accx * accx;
               cnt   <= cnt + 1;
               state <= AccYSqare;
            
            when AccYSqare =>
               accy2 <= accy * accy;
               cnt   <= cnt + 1;
               state <= AccZSqare;
            
            when AccZSqare =>
               accz2 <= accz * accz;
               cnt   <= cnt + 1;
               state <= AccYAngCalc1;
               
            when AccYAngCalc1 =>
               sqrt_radical <= std_logic_vector(resize(accy2 + accz2, sqrt_radical'length));
               if(accz >= 0) then
                  atan2_x   <= resize(signed(sqrt_q), atan2_x'length);
               else
                  atan2_x   <= 0 - resize(signed(sqrt_q), atan2_x'length);
               end if;
               atan2_y  <= 0 - resize(accx, atan2_y'length);
               if(sqrt_DRDY = '0') then
                  if(sqrt_BSY = '0') then
                     sqrt_tick <= '1';
                  else
                     sqrt_tick <= '0';
                  end if;
               else
                  state <= AccYAngCalc2;
               end if;
               cnt      <= cnt + 1;
               
            when AccYAngCalc2 =>
               if(atan2_y > 0) then
                  if(atan2_x > 0) then
                     if(abs(atan2_x) > abs(atan2_y)) then
                        --a=  0   + atan(y/x) |x|>|y|, x>0, y>0
                     elsif(abs(atan2_x) < abs(atan2_y)) then
                        --a=  90  - atan(x/y) |x|<|y|, x>0, y>0
                     else
                        --a=  45 |x|=|y|, x>0, y>0
                     end if;
                  elsif(atan2_x < 0) then
                     if(abs(atan2_x) > abs(atan2_y)) then
                        --a=  180 - atan(y/x) |x|>|y|, x<0, y>0
                     elsif(abs(atan2_x) < abs(atan2_y)) then
                        --a=  90  + atan(x/y) |x|<|y|, x<0, y>0
                     else
                        --a=  135 |x|=|y|, x<0, y>0
                     end if;
                  else --atan2_x = 0
                     --a=  90 x=0, y>0
                  end if;
               elsif(atan2_y < 0) then
                  if(atan2_x > 0) then
                     if(abs(atan2_x) > abs(atan2_y)) then
                        --a=  0   - atan(y/x) |x|>|y|, x>0, y<0
                     elsif(abs(atan2_x) < abs(atan2_y)) then
                        --a= -90  + atan(x/y) |x|<|y|, x>0, y<0
                     else
                        --a= -45 |x|=|y|, x>0, y<0
                     end if;
                  elsif(atan2_x < 0) then
                     if(abs(atan2_x) > abs(atan2_y)) then
                        --a= -180 + atan(y/x) |x|>|y|, x<0, y<0
                     elsif(abs(atan2_x) < abs(atan2_y)) then
                        --a= -90  - atan(x/y) |x|<|y|, x<0, y<0
                     else
                        --a= -135 |x|=|y|, x<0, y<0
                     end if;
                  else --atan2_x = 0
                     --a= -90 x=0, y<0
                  end if;
               else --atan2_y = 0
                  if(atan2_x > 0) then
                     --a= 0 x>0, y=0
                  elsif(atan2_x < 0) then
                     --a= 180 x<0, y=0
                  else --atan2_x = 0
                     --a= 0 x=0, y=0
                  end if;
               end if;
--               accyang  <= resize(signed(atan2_q&"000000"), accyang'length);
               cnt      <= cnt + 1;
               state <= AccYRadToDeg;
               --state <= AccXAngCalc;
--               
--            when AccXAngCalc =>
----               atan2_x  <= std_logic_vector(accz);
--               sqrt_radical <= std_logic_vector(resize(accx2 + accz2, sqrt_radical'length));
--               if(accz >= 0) then
--                  atan2_x   <= std_logic_vector(resize(signed(sqrt_q), atan2_x'length));
--               else
--                  atan2_x   <= std_logic_vector(0 - resize(signed(sqrt_q), atan2_x'length));
--               end if;
--               atan2_y  <= std_logic_vector(accy);
----               accxang  <= resize(signed(atan2_q&"000000"), accyang'length);
--               cnt      <= cnt + 1;
--               if(cnt > cntpr + 40) then
--                  state <= AccYRadToDeg;
--               end if;
               
            when AccYRadToDeg =>
               accyang  <= resize(shift_right(accyang*7509888, 16), accyang'length);
               cnt      <= cnt + 1;
               state    <= AccXRadToDeg;
               
            when AccXRadToDeg =>
               accxang  <= resize(shift_right(accxang*7509888, 16), accxang'length);
               cnt      <= cnt + 1;
               state    <= KalmanXst0;
               
            when KalmanXst0 =>
               K_angle_x <= K_angle_x + gyrx - K_bias_x;
               P00_x <= P00_x + P11_x - P01_x - P10_x + Q_angle;
               P01_x <= P01_x - P11_x;
               P10_x <= P10_x - P11_x;
               P11_x <= P11_x + Q_bias;
               cnt      <= cnt + 1;
               cntpr    <= cnt + 1;
               state    <= KalmanXst1;
               
            when KalmanXst1 =>
               S_x      <= P00_x + R_measure;
               numer    <= std_logic_vector(shift_left(resize(P00_x, P00_x'length + 16), 16));
               denom    <= std_logic_vector(S_x);
               K0_x     <= resize(signed(quotient), 32);
               cnt      <= cnt + 1;
               if(cnt > cntpr + 40) then
                  cntpr    <= cnt + 1;
                  state    <= KalmanXst2;
               end if;
               
            when KalmanXst2 =>
               numer    <= std_logic_vector(shift_left(resize(P10_x, P00_x'length + 16), 16));
               denom    <= std_logic_vector(S_x);
               K1_x     <= resize(signed(quotient), 32);
               cnt      <= cnt + 1;
               if(cnt > cntpr + 40) then
                  state    <= KalmanXst3;
               end if;
               
            when KalmanXst3 =>
               accxang_prv <= accxang;
               y_x      <= shift_right(accxang + accxang_prv, 1) - K_angle_x;
               cnt      <= cnt + 1;
               state    <= KalmanXst4;
               
            when KalmanXst4 =>
               K_angle_x <= K_angle_x + resize(shift_right(K0_x * y_x, 16), K_angle_x'length);
               cnt      <= cnt + 1;
               state    <= KalmanXst5;
               
            when KalmanXst5 =>
               K_bias_x <= K_bias_x + resize(shift_right(K1_x * y_x, 16), K_bias_x'length);
               cnt      <= cnt + 1;
               state    <= KalmanXst6;
               
            when KalmanXst6 =>
               P00_x <= resize(shift_right((to_signed(65536, K0_x'length) - K0_x) * P00_x, 16), P00_x'length);
               cnt      <= cnt + 1;
               state    <= KalmanXst7;
               
            when KalmanXst7 =>
               P01_x <= resize(shift_right((to_signed(65536, K0_x'length) - K0_x) * P01_x, 16), P01_x'length);
               cnt      <= cnt + 1;
               state    <= KalmanXst8;
               
            when KalmanXst8 =>
               P10_x <= P10_x - resize(shift_right(K1_x * P00_x, 16), P10_x'length);
               cnt      <= cnt + 1;
               state    <= KalmanXst9;
               
            when KalmanXst9 =>
               P11_x <= P11_x - resize(shift_right(K1_x * P01_x, 16), P11_x'length);
               cnt      <= cnt + 1;
               state    <= SensorXOut;
               
            when SensorXOut =>
               angx_o   <= resize(shift_right(K_angle_x*182, 16), angx_o'length);
               cnt      <= cnt + 1;
               state    <= SensorYOut;
               
            when SensorYOut =>
               angy_o   <= resize(shift_right(K_angle_y*182, 16), angy_o'length);
               cnt      <= cnt + 1;
               state    <= SensorZOut;
               
            when SensorZOut =>
               angz_o   <= resize(shift_right(angz*182, 16), angz_o'length);
               cnt      <= cnt + 1;
               DRDY     <= '1';
               state    <= wait_for_new_data;   
               
            when wait_for_new_data =>
               DRDY     <= '0';
               if(cnt < 6250) then
                  cnt   <= cnt + 1;
               else
                  cnt   <= 0;
                  state <= MPU_read;
               end if;
            
            when others =>
               state          <= who_am_I_ask;
               cnt            <= 0;
               cntpr          <= 0;
               cntoff         <= 0;
               DataOut        <= (others => '0');
               BitLen         <= (others => '0');
               DataLen        <= (others => '0');
               SPIStart       <= '0';
               accx           <= (others => '0');
               accy           <= (others => '0');
               accz           <= (others => '0');
               accx2          <= (others => '0');
               accy2          <= (others => '0');
               accz2          <= (others => '0');
               accxang        <= (others => '0');
               accyang        <= (others => '0');
               accxang_prv    <= (others => '0');
               accyang_prv    <= (others => '0');
               gyrx           <= (others => '0');
               gyry           <= (others => '0');
               gyrz           <= (others => '0');
               angx_o         <= (others => '0');
               angy_o         <= (others => '0');
               angz_o         <= (others => '0');
               gyrxoff        <= (others => '0');
               gyryoff        <= (others => '0');
               gyrzoff        <= (others => '0');
               div_tick       <= '0';
               div_numer      <= (others => '0');
               div_denom      <= (others => '0');
               sqrt_radical   <= (others => '0');
               sqrt_tick		<= '0';
               atan2_x        <= (others => '0');
               atan2_y        <= (others => '0');
               K_angle_x      <= (others => '0');
               K_bias_x       <= (others => '0');
               P00_x          <= (others => '0');
               P01_x          <= (others => '0');
               P10_x          <= (others => '0');
               P11_x          <= (others => '0');
               S_x            <= (others => '0');
               K0_x           <= (others => '0');
               K1_x           <= (others => '0');
               y_x            <= (others => '0');
               K_angle_y      <= (others => '0');
               K_bias_y       <= (others => '0');
               P00_y          <= (others => '0');
               P01_y          <= (others => '0');
               P10_y          <= (others => '0');
               P11_y          <= (others => '0');
               S_y            <= (others => '0');
               K0_y           <= (others => '0');
               K1_y           <= (others => '0');
               y_y            <= (others => '0');
               angz           <= (others => '0');
               DRDY           <= '0';
         end case;
      end if;
   end process;
   
end;

--a=  0   + atan(y/x) |x|>|y|, x>0, y>0
--a=  90  - atan(x/y) |x|<|y|, x>0, y>0
--a=  90  + atan(x/y) |x|<|y|, x<0, y>0
--a=  180 - atan(y/x) |x|>|y|, x<0, y>0
--a= -180 + atan(y/x) |x|>|y|, x<0, y<0
--a= -90  - atan(x/y) |x|<|y|, x<0, y<0
--a= -90  + atan(x/y) |x|<|y|, x>0, y<0
--a=  0   - atan(y/x) |x|>|y|, x>0, y<0