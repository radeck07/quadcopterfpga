
--Name:     Sqrt_TB
--Author:   Radoslaw Kalinka
--Date:     2017.10.28
--Desc:     Sqrt module testbench
--          Module works with 50MHz input clock
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.ALL; 

entity Sqrt_TB is
end;

architecture bhv of Sqrt_TB is
component Sqrt is
   PORT(
      i_clk       : IN STD_LOGIC;
      i_rst       : IN STD_LOGIC;
      i_tick      : IN std_logic;        --start division
      iv_input    : IN std_logic_vector; --numerator
      ov_sqrt     : OUT std_logic_vector;--result
      o_DRDY      : OUT std_logic;
      o_BSY       : OUT std_logic
   );
end component;
   
   signal clk         : std_logic;                  
   signal rst         : std_logic;                  
   signal tick      : std_logic;
   signal Result   : std_logic_vector(47 downto 0);
   signal DRDY      : std_logic;
   signal BSY       : std_logic;
   
   
begin
                           
   Sqrt_inst: Sqrt port map (
                           i_clk       => clk,
                           i_rst       => rst,
                           i_tick      => tick,
						   iv_input    => std_logic_vector(to_unsigned(12345, 16)),
                           ov_sqrt   => Result,
                           o_DRDY      => DRDY,
                           o_BSY       => BSY
                         );  
   
   
   clk_gen: process                
   begin                           
      clk <= '0';                 
      wait for 10 ns;             
      clk <= '1';                 
      wait for 10 ns;
   end process;
   
   stim_process: process
   begin
      rst <= '1';
      tick <= '0';
      wait for 20 ns;
      rst <= '0';
      wait for 20 ns;
      tick  <= '1';
      wait for 20 ns;
      tick  <= '0';
	  
      wait;
   end process;
   
end bhv;

