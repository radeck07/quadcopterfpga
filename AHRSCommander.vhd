--Name:     UART_TX
--Author:   Radoslaw Kalinka
--Date:     2016.10.08
--Desc:     AHRS Commander module. Initializes external AHRS module.
--          Module works with 50MHz input clock
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.ALL; 

entity AHRSCommander is
   PORT(
         i_clk       :  IN    std_logic;
         i_rst       :  IN    std_logic;
         i_bsy       :  IN    std_logic;
         i_restart   :  IN    std_logic;
         ov_data_out :  OUT   std_logic_vector(7 downto 0);
         o_send      :  OUT   std_logic
      );
end;

architecture bhv of AHRSCommander is
   type StateType is (first, second, third, fourth, fifth, sixth, stop);   --State machine states typedef
   signal state : StateType:=first;                --State machine declaration
   signal data_out   : std_logic_vector(7 downto 0);
   signal send       : std_logic;
begin
   ov_data_out <= data_out;
   o_send      <= send;
   
   process(i_clk, i_rst)
   begin
      if(i_rst = '1') then
         state    <= first;
         send     <= '0';
         data_out <= (others => '0');
      elsif(rising_edge(i_clk)) then
         if(i_bsy = '0') then
            if(send = '0') then
               case state is
                  when first =>
                     data_out <= "10100101"; --A5
                     send <= '1';
                     state <= second;
                  
                  when second =>
                     data_out <= "10100101"; --A5 --data_out <= "10100110"; --A6
                     send <= '1';
                     state <= third;
                  
                  when third =>
                     data_out <= "01001010"; --4A --data_out <= "01001011"; --4B
                     send <= '1';
                     state <= fourth;
                  
                  when fourth =>
                     data_out <= "10100101"; --A5
                     send <= '1';
                     state <= fifth;
                  
                  when fifth =>
                     data_out <= "01010101"; --55
                     send <= '1';
                     state <= sixth;
                  
                  when sixth =>
                     data_out <= "11111010"; --FA
                     send <= '1';
                     state <= stop;
                  
                  when stop =>
                     if(i_restart = '1') then
                        state <= first;
                     end if;
               end case;
            end if;
         else
            send <= '0';
         end if;
      end if;
   end process;
end bhv;