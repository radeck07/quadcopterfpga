--Name:     RemoteControlPPM
--Author:   Radoslaw Kalinka
--Date:     2017.05.20
--Desc:     RemoteControlPPM module that measures input PPM signal from Remote Control radio link and converts it into i16q16
--          Module works with 50MHz input clock

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_misc.all;
use ieee.numeric_std.ALL;
library work;
use work.regs.all; 

entity RemoteControlPPM is
   GENERIC(
      G_CHANNELS_N   : integer := 10
   );
   PORT(
      i_clk          : IN std_logic;
      i_rst          : IN std_logic;
      i_enable       : IN std_logic;
      iv_CHPPM       : IN std_logic_vector(10 downto 1);
      ot_PPMCH			: OUT RemoteControlPPMOutput
   );
end;

architecture bhv of RemoteControlPPM is
   type val_type  is array (1 to G_CHANNELS_N) of s2q16;
   type cnt_type  is array (1 to G_CHANNELS_N) of unsigned(16 downto 0);
   type ch_type   is array (1 to G_CHANNELS_N) of std_logic;
   type fcnt_type is array (1 to G_CHANNELS_N) of unsigned(5 downto 0);
   signal CHVal   : val_type;
   signal CHCnt   : cnt_type;
   signal CHPPM   : ch_type;
   signal CHPPMR  : ch_type;
   signal CHPPM1  : ch_type;
   signal CHPPM2  : ch_type;
   signal CHFCNT  : fcnt_type;
   signal seq     : integer range 1 to G_CHANNELS_N;
   
begin

OUTPUT_GEN: for i in 1 to 10 generate

	OUTPUT_RANGE: if i <= G_CHANNELS_N generate
		ot_PPMCH(i) <= CHVal(i);
	end generate OUTPUT_RANGE;
	
	OUTPUT_OutRANGE: if i > G_CHANNELS_N generate
		ot_PPMCH(i) <= (others => '0');
	end generate OUTPUT_OutRANGE;

end generate OUTPUT_GEN;
   
   process(i_rst, i_clk)
   begin
      if(i_rst = '1') then
         CHVal    <= (others => (others => '0'));
         CHCnt    <= (others => (others => '0'));
         CHPPM    <= (others => '0');
         CHPPM1   <= (others => '0');
         CHPPM2   <= (others => '0');
         CHFCNT   <= (others => (others => '0'));
         seq      <= 1;
      elsif(rising_edge(i_clk)) then
         if(i_enable = '1') then
            for i in 1 to G_CHANNELS_N loop
               --synchronizer chain
               CHPPM1(i)  <= iv_CHPPM(i);
               CHPPM2(i)  <= CHPPM1(i);
               
               --filter
               if(CHPPM2(i) = CHPPM(i)) then
                  CHFCNT(i)  <= (others => '0');
               elsif(CHFCNT(i) < 50) then
                  CHFCNT(i)  <= CHFCNT(i) + 1;
               else
                  CHFCNT(i)  <= (others => '0');
                  CHPPM(i)   <= not CHPPM(i);
               end if;
               
               --reg for prev val
               CHPPMR(i)  <= CHPPM(i);
               
               --length counter
               if(CHPPMR(i) = '0' AND CHPPM(i) = '1') then
                  CHCnt(i)   <= to_unsigned(1, CHCnt(i)'length);
               elsif(CHPPM(i) = '1') then
                  if(CHCnt(i) < 100000) then
                     CHCnt(i)   <= CHCnt(i) + 1;
                  end if;
               end if; 
            end loop;
            
            if(seq < G_CHANNELS_N) then
               seq <= seq + 1;
            else
               seq <= 1;
            end if;
            
            --przeliczenie na i16q16
            if(CHPPM(seq) = '0') then
               if(CHCnt(seq) > 50000) then
                  CHVal(seq)   <= signed(resize(shift_right((CHCnt(seq)-to_unsigned(50000, CHCnt(seq)'length))*85899, 16), CHVal(seq)'length));
               else
                  CHVal(seq) <= (others => '0');
               end if;
            end if; 
            
         else
            CHVal    <= (others => (others => '0'));
            CHCnt    <= (others => (others => '0'));
            CHPPM    <= (others => '0');
            CHPPM1   <= (others => '0');
            CHPPM2   <= (others => '0');
            CHFCNT   <= (others => (others => '0'));
            seq      <= 1;
         end if;
      end if;
   end process;
   
end;	