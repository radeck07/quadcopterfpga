--Name:     UART_TX
--Author:   Radoslaw Kalinka
--Date:     2016.10.08
--Desc:     Uart transmitter module that sends input data 
--          Module works with 50MHz input clock
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.ALL; 

entity UART_TX is
   GENERIC(
      BIT_LENGTH     :integer := 434                        --Bit length in clk tacts
   );
   PORT(
      i_clk       : IN    std_logic;                        --input clk
      i_rst       : IN    std_logic;                        --reset signal
      iv_data_in  : IN    std_logic_vector(7 downto 0);     --data input
      i_send      : IN    std_logic;                        --send trigger
      o_bsy       : OUT   std_logic;                        --busy flag
      o_TX        : OUT   std_logic                         --module output
   );
end;

architecture bhv of UART_TX is
   type     StateType is (ready, start, transmit, stop);    --State machine states typedef
   signal   state    : StateType :=ready;                   --State machine declaration
   signal   timer    : integer range 0 to BIT_LENGTH+1;     --Timer for time measurement
   signal   data     : std_logic_vector(7 downto 0);        --Data shift register
   signal   bit_nr   : integer range 0 to 8;                --Bit number counter
   
begin
   process(i_clk, i_rst)
   begin
      if(i_rst = '1') then
         state <= ready;
         o_TX  <= '1';
         o_bsy <= '0';
         timer <= 0;
         data  <= (others => '0');
      elsif(rising_edge(i_clk)) then
         case state is
            when ready =>
               o_bsy <= '0';
               o_TX  <= '1';
               timer <= 0;
               if(i_send = '1') then
                  state <= start;
                  data  <= iv_data_in;
                  o_bsy <= '1';
               end if;

            when start =>
               o_bsy    <= '1';
               o_TX     <= '0';
               bit_nr   <= 0;
               --if(timer = 2604) then
               if(timer = BIT_LENGTH) then
                  timer <= 0;
                  state <= transmit;
               else
                  timer <= timer + 1;
               end if;
            
            when transmit =>
               o_bsy <= '1';
               o_TX  <= data(0);
               --if(timer = 2604) then
               if(timer = BIT_LENGTH) then
                  timer    <= 0;
                  bit_nr   <= bit_nr + 1;
                  if(bit_nr = 7) then
                     state <= stop;
                  else
                     data  <= '0' & data(7 downto 1);
                  end if;
               else
                  timer <= timer + 1;
               end if;
            
            when stop =>
               o_bsy <= '1';
               o_TX  <= '1';
               --if(timer = 3000) then
               if(timer = BIT_LENGTH) then
                  if(i_send = '0') then
                     state <= ready;
                  else
                     state <= stop;
                  end if;
               else
                  timer <= timer + 1;
               end if;
         end case;
      end if;
   end process;
end bhv;